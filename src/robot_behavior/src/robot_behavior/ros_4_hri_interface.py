#!/usr/bin/env python3

from py_spring_hri import HRIListener

class ROS4HRIInterface:
    def __init__(self):
        self.hri_listener = HRIListener(
            subscribe_persons = True,
            subscribe_bodies = True,
            subscribe_groups = True,
            default_target_frame='map')
        self.hri_listener.start()
            
    
    def get_data(self):
        body_person_map = {}
        person_body_map = {}
        bodies_data = {}
        groups_data = {}

        with self.hri_listener.tracked_persons.lock:
            for person_id, person in self.hri_listener.tracked_persons.items():
                if person.body:
                    body_person_map[person.body.id] = person_id
                    person_body_map[person_id] = person.body.id

        with self.hri_listener.bodies.lock:
            for body_id, body in self.hri_listener.bodies.items():
                person_id = body_person_map.get(body_id, None)                    
                bodies_data[body_id] = (body.transform(), person_id)

        with self.hri_listener.groups.lock:
            for group_id, group in self.hri_listener.groups.items():
                add_group = True
                bodies_data_tmp = {}
                # to modify when faces-bodies-voices-persons-groups tracked will publish correctly
                if group.member_ids:
                    for member in group.member_ids:
                        if member not in person_body_map or person_body_map[member] not in bodies_data:
                            add_group = False
                        else:
                            bodies_data_tmp[person_body_map[member]] = (*bodies_data[person_body_map[member]], group_id)
                    if add_group:
                        groups_data[group_id] = (group.transform(), group.member_ids)
                        bodies_data.update(bodies_data_tmp)
                        
        # bodies_data : for each body_id : transform, person_id, group_id if group
        # groups_data : for each group_id : transform, group member_ids
        # body_person_map : for each body_id gets person_id
        # person_body_map : for each person_id gets body_id
        return bodies_data, groups_data, person_body_map, body_person_map
    

    def close(self):
        self.hri_listener.close()
        

