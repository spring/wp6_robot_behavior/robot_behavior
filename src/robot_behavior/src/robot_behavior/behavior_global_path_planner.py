# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2023 by Inria
# Authors : Victor Sanchez
# victor.sanchez@inria.fr
#
# Code inspired from : https://gitlab.inria.fr/spring/wp6_robot_behavior/Motor_Controller/-/blob/master/social_mpc/path.py?ref_type=heads
#   From : Alex Auternaud, Timothée Wintz
#   alex.auternaud@inria.fr
#   timothee.wintz@inria.fr

import cv2
import skfmm
import numpy.ma as ma
import numpy as np
from robot_behavior.utils import constraint_angle, optimal_path_2d
import rospy
import exputils as eu


class GlobalPathPlanner():

    def __init__(self, waypoint_resolution_in_meters=0.5, threshold_distance_arrival=1.0, threshold_distance_orientation=0.5, max_target_angle=np.pi/4):

        self.state_config = None
        self.threshold_distance_arrival = threshold_distance_arrival
        self.max_target_angle = max_target_angle
        self.threshold_distance_orientation = threshold_distance_orientation
        self.waypoint_resolution_in_meters = waypoint_resolution_in_meters
        self.distance = None
        self.ssn = None
        self.global_map_with_waypoints = None
        self.map_with_subgoals=None


    def set_state_config(self,
                        global_map_size=[[-11.26118716597557, 12.588813189417124], [-10.207198709249496, 14.492801658809185]],
                        global_map_height=494,
                        global_map_width=477,
                        global_map_scale=1/0.05
        ):
        self.state_config=eu.AttrDict(global_map_size=global_map_size,
                                    global_map_height=global_map_height,
                                    global_map_width=global_map_width,
                                    global_map_scale=global_map_scale
        )
        self.y_coordinates = np.linspace(global_map_size[1][0], global_map_size[1][1], self.state_config.global_map_height)
        self.x_coordinates = np.linspace(global_map_size[0][0], global_map_size[0][1], self.state_config.global_map_width)
        self.map_with_subgoals = np.zeros((global_map_width, global_map_height))


    def set_global_map_for_waypoints(self, map):
        self.global_map_with_waypoints = map


    def position_to_px_coord(self, pos):
        r''' Tranform real position to coordinate in pixel in the global map coordinate system '''
        if self.state_config is None:
            return None
        scale = self.state_config.global_map_scale
        x_min = self.state_config.global_map_size[0][0]
        y_max = self.state_config.global_map_size[1][1]
        
        i = (y_max - pos[1]) * scale
        j = (pos[0] - x_min) * scale

        return int(i), int(j)


    def px_coord_grid(self):
        r''' Tranform coordinate in pixel in the global map to real position coordinate '''
        if self.state_config is None:
            return None
        scale = self.state_config.global_map_scale
        x_min = self.state_config.global_map_size[0][0]
        y_min = self.state_config.global_map_size[1][0]

        x = [x_min + (j + 0.5) /
             scale for j in range(self.state_config.global_map_width)]
        y = [y_min + (i + 0.5) /
             scale for i in range(self.state_config.global_map_height)]
        return x, y


    def get_distance_map(self, global_map, big_number=100):
        r''' Set the distance map from sffmm 
        [see https://pythonhosted.org/scikit-fmm/ for more info about distance map]
        '''
        if self.state_config is None:
            return None
        t_i, t_j = self.position_to_px_coord(self.target)
        phi = ma.MaskedArray(np.ones(global_map.shape), mask=(global_map > 0))
        phi.data[t_i, t_j] = 0.0
        dx = 1/self.state_config.global_map_scale
        self.distance = skfmm.distance(phi, dx=dx)


    def get_waypoints(self, pos):
        r''' Return the next waypoint position from a given position 
            pos : [x,y,yaw] '''
        #x, y = self.px_coord_grid()
        if self.state_config is None:
            return None, None, None, None, None
        # x, y = self.ssn.x_coordinates, self.ssn.y_coordinates
        x, y = self.x_coordinates, self.y_coordinates
        
        optimal_path_2d_output = optimal_path_2d(
            travel_time = np.flip(self.distance, axis=0), 
            starting_point = pos[:2],
            dx=self.waypoint_resolution_in_meters,
            coords=(x, y),
            max_d_orientation=0.3)
        
        if optimal_path_2d_output :
            xl, yl, vxl, vyl, dl = optimal_path_2d_output
        else :
            return None
        if len(xl) == 0 or len(yl) == 0 or len(vxl) == 0 or len(vyl) == 0:
            return None
        else :
            return xl, yl, vxl, vyl, dl


    def run(self, global_map, target_pose, robot_pose):
        
        
        if robot_pose is None or (target_pose is None or global_map is None):  # not ready
            if robot_pose is None :
                rospy.logwarn('Warning robot_pose is None')
            if target_pose is None :
                rospy.logwarn('Warning target_pose is None')
            if global_map is None :
                rospy.logwarn('Warning global_map is None')
            self.distance = None
            return None
        

        self.target = [target_pose[0], target_pose[1], target_pose[2]]
        robot_pose = [robot_pose[0], robot_pose[1], robot_pose[2]]
        
        # if np.linalg.norm(np.array(robot_pose[0:2]) - np.array(self.target[0:2]))<self.waypoint_resolution_in_meters:
        #     return self.target[0:2]
        
        self.get_distance_map(global_map)

        get_waypoint_output = self.get_waypoints(robot_pose)

        if get_waypoint_output :
            xl, yl, vxl, vyl, dl = get_waypoint_output
            
            # Victor's method
            # wpt_pos_closest = [xl[0], yl[0]]
            
            # Alex's method
            target_dist = np.linalg.norm(np.array(robot_pose[:2]) - np.array(self.target[:2]))
            waypoint_index = min(int(self.threshold_distance_arrival / self.waypoint_resolution_in_meters), len(dl)-1)  # get the index of the next waypoint at threshold_distance_arrival with self.waypoint_resolution_in_meters step
            if target_dist < self.threshold_distance_orientation:  # if we are near the target, we only turn on ourself
                wpt_pos_closest = [robot_pose[0], robot_pose[1], self.target[2]]
            else :
                angle = np.abs(constraint_angle(np.arctan2(yl[waypoint_index] - robot_pose[1], xl[waypoint_index] - robot_pose[0]) - robot_pose[2])) 
                #  if the angle to the next point is too big, also first turn on ourself to its direction
                if angle > self.max_target_angle:
                        wpt_pos_closest = [robot_pose[0], robot_pose[1], np.arctan2(yl[waypoint_index] - robot_pose[1], xl[waypoint_index] - robot_pose[0])]
                else : 
                    if target_dist < self.threshold_distance_arrival: # we are near the target
                        wpt_pos_closest = [self.target[0],
                                           self.target[1],
                                           np.arctan2(yl[-1] - robot_pose[1], xl[-1] - robot_pose[0])]
                    else:  # nominal case else, point on shortest path with orientation towards target
                        wpt_pos_closest = [xl[waypoint_index], yl[waypoint_index], np.arctan2(yl[waypoint_index] - robot_pose[1], xl[waypoint_index] - robot_pose[0])]

            self.get_map_with_subgoals(waypoints=(xl, yl))
            return wpt_pos_closest
        else :
            return None


    def get_map_with_subgoals(self, waypoints):
        #self.global_map_with_waypoints = np.where(self.global_map_with_waypoints>0, 0 ,1)
        self.global_map_with_waypoints = np.zeros((self.state_config.global_map_height, self.state_config.global_map_width))
        for id in range(len(waypoints[0])):
            if str(waypoints[0][id]) != 'nan':
                x_px, y_px = self.position_to_px_coord(pos=(waypoints[0][id],waypoints[1][id]))
                x_px = np.clip(x_px, 0, np.shape(self.global_map_with_waypoints)[0]-1)
                y_px = np.clip(y_px, 0, np.shape(self.global_map_with_waypoints)[1]-1)
                self.global_map_with_waypoints[x_px][y_px] = 1
        self.global_map_with_waypoints = cv2.dilate(self.global_map_with_waypoints, kernel = np.ones((2,2)))