#!/usr/bin/env python3
import numpy as np
# from collections import namedtuple
import pkg_resources
import tf
import sys
import os
import yaml
from multiprocessing import Lock
import numpy as np
import rospy
from nav_msgs.msg import OccupancyGrid
from robot_behavior.behavior_goal_finder import GoalFinder
from robot_behavior.utils import constraint_angle, local_to_global
from robot_behavior.behavior_generator_node import RobotState
from social_mpc.config.config import ControllerConfig, RobotConfig
import time
from robot_behavior.ros_4_hri_interface import ROS4HRIInterface 
from spring_msgs.msg import GoToPosition, GoToEntity, LookAtEntity, LookAtPosition


class GoalFinderNode:
    def __init__(self):
        # os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'
        mpc_config_name = rospy.get_param('~mpc_config_name', 'None')
        # robot_config_name = rospy.get_param('~robot_config_name', 'None')
        mpc_config_path = None if mpc_config_name == 'None' else mpc_config_name
        # robot_config_path = None if robot_config_name == 'None' else robot_config_name
        self.mpc_config_path = mpc_config_path
        # self.robot_config_path = robot_config_path
        # self.namespace_slam = rospy.get_param('~namespace_slam', '/slam')
        self.map_frame = rospy.get_param('~map_frame', 'map')
        self.robot_frame = rospy.get_param('~robot_frame', 'base_footprint')
        self.global_occupancy_map_topic = rospy.get_param('~global_occupancy_map_topic', '/slam/global_map_rl_navigation')
        self.max_humans_world = rospy.get_param('max_humans_world', 20)
        self.max_groups_world = rospy.get_param('max_groups_world', 10)
        self.namespace = rospy.get_param('namespace', 'behavior_generator')
        
        # self.read_robot_config(filename=self.robot_config_path)
        self.read_config(filename=self.mpc_config_path)
        self.goal_finder = GoalFinder(
            controller_config=self.controller_config)

        self.global_map_data = None
        self.rtabmap_ready = False
        self.global_map_static = None
        self.hri_data = None

        self.human_id_default = -1
        self.group_id_default = -1

        self.x_final = 0.
        self.y_final = 0.
        self.yaw_final = 0.
        self.flag_final_goal_okay = False

        self.robot_x_position = 0.
        self.robot_y_position = 0.
        self.robot_yaw = 0.
        self.robot_position = [0, 0]
        self.lock_data_entrance = Lock()
    
        self.init_ros_subscriber_and_publicher()

        self.tf_broadcaster = tf.TransformBroadcaster()  # Publish Transform to look for human
        self.tf_listener = tf.TransformListener()  # Listen Transform to look for human

        self._check_all_sensors_ready()

        self.ros_4_hri_interface = ROS4HRIInterface()
        self.init_state()
        self.reset_go_to_data()

        rospy.loginfo("GoalFinderNode Initialization Ended")


    def read_config(self, filename=None):
        if filename is None:
            filename = pkg_resources.resource_filename(
                'social_mpc', 'config/social_mpc.yaml')
        elif os.path.isfile(filename):
            self.passed_config_loaded = True
        else:
            filename = pkg_resources.resource_filename(
                'social_mpc', 'config/social_mpc.yaml')
        config = yaml.load(open(filename), Loader=yaml.FullLoader)
        self.controller_config = ControllerConfig(config)

        self.goal_finder_enabled = self.controller_config.goal_finder_enabled
        self.path_planner_enabled = self.controller_config.path_planner_enabled
        self.update_goals_enabled = self.controller_config.update_goals_enabled
    

    def read_robot_config(self, filename=None):
        if filename is None:
            filename = pkg_resources.resource_filename(
                'sim2d', 'config/robot.yaml')
            rospy.logdebug("No filename provided for the robot configuration, basic robot config loaded")
        elif os.path.isfile(filename):
            rospy.logdebug("Desired robot config loaded")
        else:
            filename = pkg_resources.resource_filename(
                'sim2d', 'config/robot.yaml')
            rospy.logdebug("Desired filename for the robot configuration does not exist, basic robot config loaded")
        config = yaml.load(open(filename), Loader=yaml.FullLoader)
        self.robot_config = RobotConfig(config)


    def init_ros_subscriber_and_publicher(self):
        r''' Initialize the subscribers and publishers '''
        self._subscribers = []
        self._publishers = []
        self._action_server = []
        self._timers = []

        ### ROS Subscribers
        self._subscribers.append(rospy.Subscriber(self.global_occupancy_map_topic, OccupancyGrid, callback=self._global_map_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/go_to_position', GoToPosition, callback=self._go_to_position_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/go_to_body', GoToEntity, callback=self._go_to_body_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/go_to_person', GoToEntity, callback=self._go_to_person_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/go_to_group', GoToEntity, callback=self._go_to_group_callback, queue_size=1))

        ### ROS Publishers
        # self._cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        # self._publishers.append(self._cmd_vel_pub)

        rospy.loginfo("Initializing the BehaviorGenerator")

        # self._check_publishers_connection()
 

    def run(self):
        r''' Runs the ros wrapper '''
        init_t = rospy.Time.now()
        while not rospy.is_shutdown():
            self.step(init_t)


    def step(self, init_time):
        r''' Step the ros wrapper'''

        # Localizing the robot in the environment
        self.get_robot_pose()
        self.hri_data = self.ros_4_hri_interface.get_data()
        self.recreate_state()
        
        if self.go_to_position_data:
            # no need to compute the final goal because go to position
            if self.go_to_position_data.go_to_target_pose[-1]:
                if not self.go_to_position_data.go_to_target_pose[-2]:
                    self.x_final = self.go_to_position_data.go_to_target_pose[0]
                    self.y_final = self.go_to_position_data.go_to_target_pose[1]
                    self.yaw_final = self.go_to_position_data.go_to_target_pose[2]
                else:
                    self.x_final, self.y_final = local_to_global(self.robot_position, np.asarray(self.go_to_position_data.go_to_target_pose[:2]))
                    self.yaw_final = constraint_angle(self.robot_yaw + self.go_to_position_data.go_to_target_pose[2])

        else:
            if self.go_to_body_data:
                go_to_body_target = int(self.go_to_body_data.id[1:])
                if float(go_to_body_target) in self.state.humans_id.tolist() and go_to_body_target >= 0:
                    self.shared_goto_target_human_id = [1, go_to_body_target]
            
            if self.go_to_group_data:
                go_to_group_target = int(self.go_to_group_data.id[3:])
                if float(go_to_group_target) in self.state.groups_id.tolist() and go_to_group_target >= 0:
                    self.shared_goto_target_group_id = [1, go_to_group_target]
            
            if self.go_to_person_data:
                go_to_person_target = self.go_to_person_data.id
                if go_to_person_target in self.hri_data[2].keys():
                    go_to_person_target = int(self.hri_data[2][self.go_to_person_data.id][1:])
                    if float(go_to_person_target) in self.state.humans_id.tolist():
                        self.shared_goto_target_human_id = [1, go_to_person_target]

            if self.go_to_body_data or self.go_to_group_data or self.go_to_person_data:
            # final_goal = None
                final_goal = self.goal_finder.run(
                    self.shared_humans,
                    self.shared_groups,
                    self.state.robot_pose,
                    self.shared_goto_target_human_id,
                    self.shared_goto_target_group_id,
                    self.global_map_static,
                )
                if final_goal:
                    self.flag_final_goal_okay = True
                    self.x_final, self.y_final, self.yaw_final = final_goal
                    rospy.logdebug('Robot position : x {:.2f} \t y {:.2f} \t yaw {:.2f}'.format(self.robot_x_position, self.robot_y_position, self.robot_yaw))
                    rospy.logdebug('Goal position : x {:.2f} \t y {:.2f} \t yaw {:.2f}'.format(self.x_final, self.y_final, self.yaw_final))
                else:
                    self.flag_final_goal_okay = True
                    rospy.logwarn("The final goal was not computed properly")
            
        self.publish_tf_go_to_goals()
        self.reset_go_to_data()
        rospy.sleep(self.controller_config.goal_loop_time)


    def publish_tf_go_to_goals(self):
        
        current_time = rospy.Time.now()
        quat_final = tf.transformations.quaternion_from_euler(0, 0, self.yaw_final)
        self.tf_broadcaster.sendTransform(
            (self.x_final, self.y_final, 0),
            quat_final,
            current_time,
            "final goal",
            self.map_frame
        )   


    def get_robot_pose(self):
        r""" Function that compute the position of the robot on the map """
        # try:
        #     map_2_robot = self.tf_listener.lookupTransform(self.map_frame, self.robot_frame, self.timestamp_tracking_latest)
        # except:
        try:
            map_2_robot = self.tf_listener.lookupTransform(self.map_frame, self.robot_frame, rospy.Time(0))
            #rospy.logwarn("Could not get transformation between {} and {} at correct time. Using latest available.".format(self.map_frame, self.robot_frame))
        except:
            #rospy.logerr("Could not get transformation between {} and {}.".format(self.map_frame, self.robot_frame))
            return None
        self.robot_x_position, self.robot_y_position, _ = map_2_robot[0]
        (_, _, yaw) = tf.transformations.euler_from_quaternion([map_2_robot[1][0],
                                                                map_2_robot[1][1],
                                                                map_2_robot[1][2],
                                                                map_2_robot[1][3]])
        self.robot_yaw = float(constraint_angle(yaw))
        self.robot_position = np.array([self.robot_x_position, self.robot_y_position])


    def reset_go_to_data(self):
        self.go_to_position_data = None
        self.go_to_body_data = None
        self.go_to_person_data = None
        self.go_to_group_data = None

    
    def _go_to_position_callback(self, data):
        self.reset_go_to_data()
        self.go_to_position_data = data
    

    def _go_to_body_callback(self, data):
        self.reset_go_to_data()
        self.go_to_body_data = data


    def _go_to_group_callback(self, data):
        self.reset_go_to_data()
        self.go_to_group_data = data


    def _go_to_person_callback(self, data):
        self.reset_go_to_data()
        self.go_to_person_data = data


    def _global_map_callback(self, data):
        r''' Function that computes the global map from the data and store it into the memory under self.global_map_static '''
        if data:
            with self.lock_data_entrance:
                self.global_map_data = data
                self.x_global_map = self.global_map_data.info.origin.position.x
                self.y_global_map = self.global_map_data.info.origin.position.y
                self.global_map_width = self.global_map_data.info.width
                self.global_map_height = self.global_map_data.info.height
                self.global_map_resolution = self.global_map_data.info.resolution
                self.global_map_size = [[self.x_global_map, self.x_global_map + self.global_map_width*self.global_map_resolution],[self.y_global_map, self.y_global_map + self.global_map_height*self.global_map_resolution]]
                self.last_shape_global_map = (self.global_map_height, self.global_map_width)
                self.global_map_static = np.flip((np.asarray(self.global_map_data.data) / 100).reshape(self.last_shape_global_map), axis=0)
                if self.goal_finder.state_config is None :
                    self.goal_finder.set_state_config(global_map_size=self.global_map_size,
                                                      global_map_height=self.global_map_height,
                                                      global_map_width=self.global_map_width,
                                                      global_map_scale=1/0.05
                                                     )


    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_rtabmap_ready()
        self._check_global_map_ready()
        rospy.logdebug("ALL SENSORS READY")


    def _check_global_map_ready(self):
        self.global_map_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.global_occupancy_map_topic))
        while self.global_map_data is None and not rospy.is_shutdown():
            try:
                self.global_map_data = rospy.wait_for_message(self.global_occupancy_map_topic, OccupancyGrid, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.global_occupancy_map_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for getting global map".format(self.global_occupancy_map_topic))
        return self.global_map_data


    def _check_rtabmap_ready(self):
        rospy.logdebug("Waiting for rtabmap pose to be READY...")
        while self.rtabmap_ready is None and not rospy.is_shutdown():
            try:
                self.tf_listener.waitForTransform(self.map_frame, self.robot_frame, rospy.Time(0), rospy.Duration(5.0))
                self.rtabmap_ready = True
                rospy.logdebug("Current rtabmap pose READY=>")

            except:
                rospy.logerr("Current rtabmap pose not ready yet, retrying for getting rtabmap pose")
        return self.rtabmap_ready


    def reset_state(self):
        nb_humans = self.max_humans_world
        self.state.humans_id = np.ones(nb_humans) * self.human_id_default
        self.state.humans_group_id = np.ones(nb_humans) * self.group_id_default
        self.state.groups_id = np.ones(self.max_groups_world) * self.group_id_default
        self.state.humans_visible = np.ones(nb_humans)
        self.state.groups_visible = np.ones(self.max_groups_world)
        self.state.humans_pose = np.zeros((nb_humans, 3))  # x, y, angle
        self.state.groups_pose = np.zeros((self.max_groups_world, 2))  # x, y
        self.state.humans_velocity = np.zeros((nb_humans, 2))
        self.state.robot_pose = np.zeros(3)
        self.state.robot_velocity = np.zeros(2)
        # if self.state.robot_config.has_pan and self.state.robot_config.has_tilt:
        #     self.state.joint_angles = np.zeros(2)
        #     self.state.joint_velocities = np.zeros(2)
        # elif not self.state.robot_config.has_pan and not self.state.robot_config.has_tilt:
        #     self.state.joint_angles = None
        #     self.state.joint_velocities = None
        # else:
        #     self.state.joint_angles = np.zeros(1)
        #     self.state.joint_velocities = np.zeros(1)

        self.shared_humans = np.zeros((nb_humans, 7))  # x, y, orientation, lin vel, ang vel, group id, id
        self.shared_groups = np.zeros((self.max_groups_world, 3))  # x, y, id
        self.shared_goto_target_group_id = np.zeros(2)  # flag, id
        self.shared_goto_target_human_id = np.zeros(2)  # flag, id


    def recreate_state(self):
        self.reset_state()

        self.state.robot_pose = np.array([self.robot_x_position, self.robot_y_position, self.robot_yaw])
        # self.state.robot_velocity = np.array([lin, ang])

        # self.state.joint_angles = np.array([self.joint_states_data.position[self.pan_ind]])
        # self.state.joint_velocities = np.array([self.joint_states_data.velocity[self.pan_ind]])
        
        for human_id, human_data in self.hri_data[0].items():
            human_id = int(human_id[1:])
            humans_id_list = self.state.humans_id.tolist()
            if human_id in humans_id_list:
                index = humans_id_list.index(human_id)
            elif self.human_id_default in humans_id_list:
                index = humans_id_list.index(self.human_id_default)
            else:
                rospy.logerr("No more space in state.humans_ arrays. Number max is {}. It is fixed because of the use of SharedMemory multiprocessing".format(self.max_humans_world))
            self.state.humans_id[index] = human_id
            self.state.humans_visible[index] = 0.  # how to get it ?
            if len(human_data) >= 3:
                self.state.humans_group_id[index] = int(human_data[2][3:])  # 0: transform, 1:person_id, 2:group_id if group

            human_pose = human_data[0].transform.translation
            human_quat = human_data[0].transform.rotation
            x_pos, y_pos = human_pose.x, human_pose.y
            (_, _, yaw) = tf.transformations.euler_from_quaternion([
                human_quat.x,
                human_quat.y,
                human_quat.z,
                human_quat.w])

            lin = 0.  # how to get it ?
            ang = 0.  # how to get it ?

            self.state.humans_pose[index, :] = [x_pos, y_pos, constraint_angle(yaw)]
            self.state.humans_velocity[index, :] = [lin, ang]

        rospy.logdebug('humans_id : {}'.format(self.state.humans_id.tolist()))

        for group_id, group_data in self.hri_data[1].items():
            group_id  = int(group_id[3:])
            groups_id_list = self.state.groups_id.tolist()
            if group_id in groups_id_list:
                index = groups_id_list.index(group_id)
            elif self.group_id_default in groups_id_list:
                index = groups_id_list.index(self.group_id_default)
            else:
                rospy.logerr("No more space in state.groups_ arrays. Number max is {}. It is fixed because of the use of SharedMemory multiprocessing".format(self.max_groups_world))
            self.state.groups_id[index] = group_id
            self.state.groups_visible[index] = 0.  # how to get it ?

            group_pose = group_data[0].transform.translation
            x_pos, y_pos = group_pose.x, group_pose.y
            self.state.groups_pose[index, :] = [x_pos, y_pos]

        rospy.logdebug('groups_id ; {}'.format(self.state.groups_id.tolist()))

        # for person_id, person_data in self.hri_data[2].items():
        #     print('person_id', person_id, 'person_data', person_data)
        # for body_id, body_data in self.hri_data[3].items():
        #    print('body_id', body_id, 'body_data', body_data)

        # self.state.config = self.Config(self.global_map_size,
        #                                 round(1/self.global_map_resolution),
        #                                 self.global_map_width,
        #                                 self.global_map_height,
        #                                 self.local_map_size,
        #                                 round(1/self.local_map_resolution),
        #                                 self.local_map_width,
        #                                 self.local_map_height)

        # self.state.global_map = self.global_map_static
        # local_map = np.zeros((*self.local_static_map.shape, 3))
        # local_map[:, :, 0] = self.local_static_map
        # local_map[:, :, 1] = self.get_social_cost_map(data=self.local_map_data)
        # self.state.local_map = local_map

        self.shared_humans[:, -1] = -1
        idx = np.where(self.state.humans_visible == 0)[0]
        n = len(idx)
        self.shared_humans[:n, -1] = self.state.humans_id[idx]
        self.shared_humans[:n, -2] = self.state.humans_group_id[idx]
        self.shared_humans[:n, :3] = self.state.humans_pose[idx, :]
        self.shared_humans[:n, 3:5] = self.state.humans_velocity[idx, :]

        self.shared_groups[:, -1] = -1
        idx = np.where(self.state.groups_visible == 0)[0]
        n = len(idx)
        self.shared_groups[:n, :2] = self.state.groups_pose[idx, :]
        self.shared_groups[:n, -1] = self.state.groups_id[idx]
    

    def init_state(self):
        self.state = RobotState()
        self.list_attr = ["robot_pose",
                          "robot_velocity",
                        #   "joint_angles",
                        #   "joint_velocities",
                        #   "local_map",
                        #   "global_map",
                          "groups_pose",
                          "humans_pose",
                          "humans_velocity",
                          "humans_visible",
                          "groups_visible",
                          "groups_id",
                          "humans_group_id",
                          "humans_id",
                        #   "config",
                        #   "robot_config"
                          ]
        for attr in self.list_attr:
            self.state.__setattr__(attr, None)

        # self.Config = namedtuple('Config', ['global_map_size',
        #                                     'global_map_scale',
        #                                     'global_map_width',
        #                                     'global_map_height',
        #                                     'local_map_size',
        #                                     'local_map_scale',
        #                                     'local_map_width',
        #                                     'local_map_height'])

        # self.RobotConfig = namedtuple('RobotConfig', ['has_pan',
        #                                               'has_tilt',
        #                                               'min_pan_angle',
        #                                               'max_pan_angle',
        #                                               'base_footprint_radius'])
        # self.state.robot_config = self.RobotConfig(self.robot_config.has_pan,
        #                                            self.robot_config.has_tilt,
        #                                            self.robot_config.min_pan_angle,
        #                                            self.robot_config.max_pan_angle,
        #                                            self.robot_config.base_footprint_radius)

        self.shared_humans = None
        self.shared_groups = None
        self.shared_goto_target_group_id = None
        self.shared_goto_target_human_id = None

        self.reset_state()


    def shutdown(self):
        rospy.loginfo("Stopping the GoalFinderNode")
        self.close()
        rospy.loginfo("Killing the GoalFinderNode node")


    def close(self):
        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()

        if self._publishers:
            for publisher in self._publishers:
                if isinstance(publisher, dict):
                    for pub in publisher.values():
                        pub.unregister()
                else:
                    publisher.unregister()
        if self._timers:
            for timer in self._timers:
                timer.shutdown()

        if self._action_server:
            self._action_server.shutdown()
        self.ros_4_hri_interface.close()