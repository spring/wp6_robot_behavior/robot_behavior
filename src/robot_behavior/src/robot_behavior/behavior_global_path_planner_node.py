#!/usr/bin/env python3
import numpy as np
import pkg_resources
import tf
import os
import yaml
import cv2
import sys
from multiprocessing import Lock
import numpy as np
import rospy
from nav_msgs.msg import OccupancyGrid, MapMetaData
from robot_behavior.behavior_global_path_planner import GlobalPathPlanner
from robot_behavior.utils import constraint_angle
from social_mpc.config.config import ControllerConfig
import time


class GlobalPathPlannerNode:
    def __init__(self):
        # os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'
        mpc_config_name = rospy.get_param('~mpc_config_name', 'None')
        mpc_config_path = None if mpc_config_name == 'None' else mpc_config_name
        self.mpc_config_path = mpc_config_path
        self.namespace_slam = rospy.get_param('~namespace_slam', '/slam')
        self.map_frame = rospy.get_param('~map_frame', 'map')
        self.robot_frame = rospy.get_param('~robot_frame', 'base_footprint')
        self.global_occupancy_map_topic = rospy.get_param('~global_occupancy_map_topic', '/slam/global_map_rl_navigation')
        
        self.read_config(filename=self.mpc_config_path)
        self.global_path_planner = GlobalPathPlanner(
            waypoint_resolution_in_meters=self.controller_config.orientation_distance_threshold,
            threshold_distance_orientation=self.controller_config.orientation_distance_threshold,
            threshold_distance_arrival=self.controller_config.waypoint_distance)

        self.global_map_data = None
        self.rtabmap_ready = False
        self.global_map_static = None

        self.goal_position = [5, 1]

        self.x_wp = 0.
        self.y_wp = 0.
        self.x_final = 0.
        self.y_final = 0.
        self.yaw_final = 0.
        self.yaw_wp = 0.
        self.flag_waypoint_okay = False


        self.robot_x_position = 0.
        self.robot_y_position = 0.
        self.robot_yaw = 0.
        self.robot_position = [0, 0]
        self.robot_position_former = [0, 0]
        self.lock_data_entrance = Lock()        
    
        self.init_ros_subscriber_and_publicher()

        self.tf_broadcaster = tf.TransformBroadcaster() # Publish Transform to look for human
        self.tf_listener = tf.TransformListener() # Listen Transform to look for human

        self._check_all_sensors_ready()

        rospy.loginfo("GlobalPathPlannerNode Initialization Ended")


    def read_config(self, filename=None):
        if filename is None:
            filename = pkg_resources.resource_filename(
                'social_mpc', 'config/social_mpc.yaml')
        elif os.path.isfile(filename):
            self.passed_config_loaded = True
        else:
            filename = pkg_resources.resource_filename(
                'social_mpc', 'config/social_mpc.yaml')
        config = yaml.load(open(filename), Loader=yaml.FullLoader)
        self.controller_config = ControllerConfig(config)

        self.goal_finder_enabled = self.controller_config.goal_finder_enabled
        self.path_planner_enabled = self.controller_config.path_planner_enabled
        self.update_goals_enabled = self.controller_config.update_goals_enabled


    def init_ros_subscriber_and_publicher(self):
        r''' Initialize the subscribers and publishers '''
        self._subscribers = []
        self._publishers = []
        self._action_server = []
        self._timers = []

        ### ROS Subscribers
        self._subscribers.append(rospy.Subscriber(self.global_occupancy_map_topic, OccupancyGrid, callback=self._global_map_callback, queue_size=1))
        self.time_per_ros_step = 2 # in sec

        ### ROS Publishers
        # self._cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        # self._publishers.append(self._cmd_vel_pub)

        ### Publishing map in Rviz
        self._global_waypoint_map_pub = rospy.Publisher(self.namespace_slam + '/global_waypoint_map', OccupancyGrid, queue_size=10, latch=True)
        self._publishers.append(self._global_waypoint_map_pub)

        rospy.loginfo("Initializing the BehaviorGenerator")

        # self._check_publishers_connection()
 

    def run(self):
        r''' Runs the ros wrapper '''
        
        # At time step 0, final goal = robot position
        self.get_robot_pose()
        self.x_final = self.robot_x_position
        self.y_final = self.robot_y_position
        self.yaw_final = self.robot_yaw
        
        init_t = rospy.Time.now()
        while not rospy.is_shutdown():
            self.step(init_t)
            

    def step(self, init_time):
        r''' Step the ros wrapper'''

        # Localizing the robot in the environment
        self.get_robot_pose()
        # Get the final goal tf
        self.get_final_goal_pose()
        if self.global_map_static is not None :
            # if np.linalg.norm(np.array(self.robot_position_former) - np.array(self.robot_position)) > 0.05 :
            # Case where the waypoints were not computed yet or when the goal position was changed 
            waypoint_target = self.global_path_planner.run(
                                                            global_map = cv2.dilate(self.global_map_static,kernel=cv2.getStructuringElement(cv2.MORPH_RECT,(8,8)),iterations = 1),
                                                            target_pose = [self.x_final, self.y_final, self.yaw_final],
                                                            robot_pose=[self.robot_x_position, self.robot_y_position, self.robot_yaw]
                            )
            if waypoint_target:
                self.flag_waypoint_okay = True
                self.x_wp, self.y_wp, self.yaw_wp = waypoint_target
                rospy.logdebug('Robot position : x {:.2f} \t y {:.2f} \t yaw {:.2f}'.format(self.robot_x_position, self.robot_y_position, self.robot_yaw))
                rospy.logdebug('Goal position : x {:.2f} \t y {:.2f} \t yaw {:.2f}'.format(self.x_final, self.y_final, self.yaw_final))
            else:
                self.flag_waypoint_okay = False
                rospy.logwarn("The waypoint was not computed properly")

            if self.global_path_planner.global_map_with_waypoints is not None :
                self.publish_global_waypoint_map(self.global_path_planner.global_map_with_waypoints)
            else :
                rospy.logwarn('waypoint map not ready !') 

            self.robot_position_former  = self.robot_position
            
            self.publish_tf_go_to_goals()
            rospy.sleep(self.controller_config.path_loop_time)


    def publish_tf_go_to_goals(self):
        
        current_time = rospy.Time.now()
        # to debug 
        # quat_final = tf.transformations.quaternion_from_euler(0, 0, self.yaw_final)
        # self.x_final, self.y_final = self.goal_position
        # self.tf_broadcaster.sendTransform(
        #     (self.x_final, self.y_final, 0),
        #     quat_final,
        #     current_time,
        #     "final goal",
        #     self.map_frame
        # )
        if self.flag_waypoint_okay:
            quat_wp = tf.transformations.quaternion_from_euler(0, 0, self.yaw_wp)
            self.tf_broadcaster.sendTransform(
                (self.x_wp, self.y_wp, 0),
                quat_wp,
                current_time,
                "intermediate waypoint goal",
                self.map_frame
            )

    
    def get_final_goal_pose(self):
        try:
            map_2_final_goal = self.tf_listener.lookupTransform(self.map_frame, 'final goal', rospy.Time(0))
            #rospy.logwarn("Could not get transformation between {} and {} at correct time. Using latest available.".format(self.map_frame, self.robot_frame))
        except:
            #rospy.logerr("Could not get transformation between {} and {}.".format(self.map_frame, self.robot_frame))
            return None
        self.x_final, self.y_final, _ = map_2_final_goal[0]
        (_, _, yaw) = tf.transformations.euler_from_quaternion([map_2_final_goal[1][0],
                                                                map_2_final_goal[1][1],
                                                                map_2_final_goal[1][2],
                                                                map_2_final_goal[1][3]])
        self.yaw_final = float(constraint_angle(yaw))       


    def get_robot_pose(self):
        r""" Function that compute the position of the robot on the map """
        # try:
        #     map_2_robot = self.tf_listener.lookupTransform(self.map_frame, self.robot_frame, self.timestamp_tracking_latest)
        # except:
        try:
            map_2_robot = self.tf_listener.lookupTransform(self.map_frame, self.robot_frame, rospy.Time(0))
            #rospy.logwarn("Could not get transformation between {} and {} at correct time. Using latest available.".format(self.map_frame, self.robot_frame))
        except:
            #rospy.logerr("Could not get transformation between {} and {}.".format(self.map_frame, self.robot_frame))
            return None
        self.robot_x_position, self.robot_y_position, _ = map_2_robot[0]
        (_, _, yaw) = tf.transformations.euler_from_quaternion([map_2_robot[1][0],
                                                                map_2_robot[1][1],
                                                                map_2_robot[1][2],
                                                                map_2_robot[1][3]])
        self.robot_yaw = float(constraint_angle(yaw))
        self.robot_position = np.array([self.robot_x_position, self.robot_y_position])

    def publish_global_waypoint_map(self, map):
        self._global_waypoint_map_pub.publish(self.set_global_map_object(map))
    
    def set_global_map_object(self, map):
        occupancy_grid = OccupancyGrid()
        occupancy_grid.info = MapMetaData()
        occupancy_grid.info.map_load_time = rospy.Time.now()
        occupancy_grid.info.resolution =  self.global_map_resolution
        occupancy_grid.info.width = self.global_map_width
        occupancy_grid.info.height = self.global_map_height
        occupancy_grid.info.origin = self.origin_global_map
        
        occupancy_grid.data = (np.flip(map, axis=0)*100).flatten().astype(np.int8)

        occupancy_grid.header.stamp = self.timestamp_global_map_cb
        occupancy_grid.header.frame_id = self.map_frame

        return occupancy_grid
    
    def _global_map_callback(self, data):
        r''' Function that computes the global map from the data and store it into the memory under self.global_map_static '''
        if data:
            with self.lock_data_entrance:
                self.global_map_data = data
                self.timestamp_global_map_cb = self.global_map_data.header.stamp
                self.x_global_map = self.global_map_data.info.origin.position.x
                self.y_global_map = self.global_map_data.info.origin.position.y
                self.origin_global_map = self.global_map_data.info.origin
                self.global_map_width = self.global_map_data.info.width
                self.global_map_height = self.global_map_data.info.height
                self.global_map_resolution = self.global_map_data.info.resolution
                self.global_map_size = [[self.x_global_map, self.x_global_map + self.global_map_width*self.global_map_resolution],[self.y_global_map, self.y_global_map + self.global_map_height*self.global_map_resolution]]
                self.last_shape_global_map = (self.global_map_height, self.global_map_width)
                self.global_map_static = np.flip((np.asarray(self.global_map_data.data) / 100).reshape(self.last_shape_global_map), axis=0)
                if self.global_path_planner.state_config is None :
                    self.global_path_planner.set_state_config(global_map_size=self.global_map_size,
                                            global_map_height=self.global_map_height,
                                            global_map_width=self.global_map_width,
                                            global_map_scale=1/0.05
                    )


    def shutdown(self):
        rospy.loginfo("Stopping the GlobalPathPlannerNode")
        self.close()
        rospy.loginfo("Killing the GlobalPathPlannerNode node")

    def close(self):
        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()

        if self._publishers:
            for publisher in self._publishers:
                if isinstance(publisher, dict):
                    for pub in publisher.values():
                        pub.unregister()
                else:
                    publisher.unregister()
        if self._timers:
            for timer in self._timers:
                timer.shutdown()

        if self._action_server :
            self._action_server.shutdown()


    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_rtabmap_ready()
        self._check_global_map_ready()
        rospy.logdebug("ALL SENSORS READY")

    def _check_global_map_ready(self):
        self.global_map_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.global_occupancy_map_topic))
        while self.global_map_data is None and not rospy.is_shutdown():
            try:
                self.global_map_data = rospy.wait_for_message(self.global_occupancy_map_topic, OccupancyGrid, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.global_occupancy_map_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for getting global map".format(self.global_occupancy_map_topic))
        return self.global_map_data
    
    def _check_rtabmap_ready(self):
        rospy.logdebug("Waiting for rtabmap pose to be READY...")
        while self.rtabmap_ready is None and not rospy.is_shutdown():
            try:
                self.tf_listener.waitForTransform(self.map_frame, self.robot_frame, rospy.Time(0), rospy.Duration(5.0))
                self.rtabmap_ready = True
                rospy.logdebug("Current rtabmap pose READY=>")

            except:
                rospy.logerr("Current rtabmap pose not ready yet, retrying for getting rtabmap pose")
        return self.rtabmap_ready