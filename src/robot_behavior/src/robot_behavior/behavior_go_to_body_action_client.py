#!/usr/bin/env python3
import rospy
import actionlib
from spring_msgs.msg import GoToEntityAction, GoToEntityGoal


class GoToBodyActionClient:
    def __init__(self, name):
        self._action_name = name
        self.body_id = rospy.get_param('~body_id', 'b-1')
        self.timeout = rospy.get_param('~timeout', 120)
        self.continuous = rospy.get_param('~continuous', False)

        rospy.on_shutdown(self.shutdown)

        # Creates the SimpleActionClient
        self._a_client = actionlib.SimpleActionClient(self._action_name, GoToEntityAction)

        # Waits until the action server has started up and started listening for goals
        self._a_client.wait_for_server()
        self._a_client.cancel_all_goals()
        rospy.sleep(0.1)

        rospy.loginfo("{} Initialization Ended".format(self._action_name))


    def call_server(self):
        goal = GoToEntityGoal()
        goal.id = self.body_id
        goal.timeout = self.timeout
        goal.continuous = self.continuous

        # Sends the goal to the action server
        self._a_client.send_goal(goal, feedback_cb=self.feedback_cb)

        # Waits for the server to finish performing the action
        self._a_client.wait_for_result()

        # Prints out the result of executing the action
        result = self._a_client.get_result()
        return result

    def feedback_cb(self, msg):
        rospy.loginfo('Feedback received: {}'.format(msg))


    def shutdown(self):
        rospy.loginfo("Stopping the %s " % self._action_name)
        self._a_client.cancel_all_goals()
        rospy.loginfo("Killing the %s node" % self._action_name)
