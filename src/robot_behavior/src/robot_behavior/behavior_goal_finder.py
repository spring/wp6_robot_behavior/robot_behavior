# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the 
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
# 
# Copyright (C) 2020-2023 by Inria
# Authors : Victor Sanchez
# victor.sanchez@inria.fr
#
# Code inspired from : https://gitlab.inria.fr/spring/wp6_robot_behavior/Motor_Controller/-/blob/master/social_mpc/path.py?ref_type=heads
#   From : Alex Auternaud, Timothée Wintz
#   alex.auternaud@inria.fr
#   timothee.wintz@inria.fr

from social_mpc.ssn_model.social_spaces import SocialSpaces, Group
from social_mpc import utils
import numpy as np
import time
import rospy
import exputils as eu


class GoalFinder():

    def __init__(self, controller_config):
        self.state_config = None
        self.ssn = None
        self.controller_config = controller_config
    
    def set_state_config(self,
                        global_map_size=[[-11.26118716597557, 12.588813189417124], [-10.207198709249496, 14.492801658809185]],
                        global_map_height=494,
                        global_map_width=477,
                        global_map_scale=1/0.05
                        ):
        self.state_config=eu.AttrDict(global_map_size=global_map_size,
                                    global_map_height=global_map_height,
                                    global_map_width=global_map_width,
                                    global_map_scale=global_map_scale
        )
        self.ssn = SocialSpaces(
            bbox=self.state_config.global_map_size,
            map_shape=(self.state_config.global_map_height,
                       self.state_config.global_map_width)
            )
    
    def run(self,
            shared_humans,
            shared_groups,
            shared_robot_pose,
            shared_goto_target_human_id,
            shared_goto_target_group_id,
            shared_global_map,
            ):
        last_time = time.time()

        if not self.ssn:
            return None
        
        global_map = (np.array([*shared_global_map])).squeeze()
        if self.controller_config.goal_loop_time:
            new_time = time.time()
            if (new_time - last_time) < self.controller_config.goal_loop_time:
                time.sleep(self.controller_config.goal_loop_time - (new_time - last_time))
            last_time = time.time()

        robot_pose = (shared_robot_pose[0], shared_robot_pose[1])
        sh_goto_target_human_id = np.array([*shared_goto_target_human_id])
        sh_goto_target_group_id = np.array([*shared_goto_target_group_id])
        if shared_humans is not None:
            sh_humans = np.array([*shared_humans])
        if shared_groups is not None:
            sh_groups = np.array([*shared_groups])
        if shared_groups is None and shared_humans is None:
            return None
        if not sh_goto_target_human_id[0] and not sh_goto_target_group_id[0]:
            return None
        n_human = np.sum(sh_humans[:, -1] >= 0)
        if n_human < 1:
            return None
        humans = np.zeros((n_human, sh_humans.shape[1]))
        for j in range(n_human):
            humans[j, :] = sh_humans[j, :]
        human_ids = np.array(sh_humans[sh_humans[:, -1] >= 0, -1], dtype=int).tolist()
        
        n_group = np.sum(sh_groups[:, -1] >= 0)
        sh_groups = sh_groups[:n_group, :]
        group_ids = np.array(sh_groups[sh_groups[:, -1] >= 0, -1], dtype=int).tolist()
        target = None

        if sh_goto_target_human_id[0]:
            if sh_goto_target_human_id[1] in human_ids:
                idx = human_ids.index(sh_goto_target_human_id[1])
                if humans[idx, -2] != -1:
                    gr_idx = group_ids.index(humans[idx, -2])
                    target = ('human_in_group', idx, gr_idx, humans[idx, -2])  # type, human id, group index, group id
                else:
                    target = ('isolated_human', idx)  # type, human id
            else:
                rospy.logwarn("Target human not in sight")
                return None
        if sh_goto_target_group_id[0]:
            if sh_goto_target_group_id[1] in group_ids:
                gr_idx = group_ids.index(sh_goto_target_group_id[1])
                target = ('group', gr_idx, sh_goto_target_group_id[1])  # type, group index, group id
            else:
                rospy.logwarn("Target group not in sight")
                return None

        groups = []
        for idx, group_id in enumerate(group_ids):
            groups.append(Group(center=sh_groups[idx, :2], person_ids=np.where(humans[:, -2] == group_id)[0]))
        
        humans_fdz = np.zeros((n_human, 5))
        humans_fdz[:, :3] = humans[:, :3]
        f_dsz = self.ssn.calc_dsz(humans_fdz, groups=groups)

        if target:
            if target[0] == 'human_in_group':
                center = [sh_groups[target[2], 0], sh_groups[target[2], 1]]
                persons = humans[humans[:, -2] == target[3], :]
                goal = self.ssn.calc_goal_pos(
                    f_dsz=f_dsz,
                    map=global_map,
                    persons=persons,
                    group_center=center,
                    robot=robot_pose)
                goal = list(goal)
                goal[2] = np.arctan2(humans[target[1]][1] - goal[1], humans[target[1]][0] - goal[0])  # to face the human
            elif target[0] == 'isolated_human':
                person = humans[target[1], :]
                ts_x = person[0] + np.cos(person[2]) * self.controller_config.group_detection_stride
                ts_y = person[1] + np.sin(person[2]) * self.controller_config.group_detection_stride
                center = [ts_x, ts_y]
                person = person[np.newaxis, :]
                goal = self.ssn.calc_goal_pos(
                    f_dsz=f_dsz,
                    map=global_map,
                    persons=person,
                    group_center=center,
                    robot=robot_pose)
                goal = list(goal)
                goal[2] = np.arctan2(humans[target[1]][1] - goal[1],humans[target[1]][0] - goal[0])  # to face the human
            elif target[0] == 'group':
                center = [sh_groups[target[1], 0], sh_groups[target[1], 1]]
                persons = humans[humans[:, -2] == target[2], :]
                goal = self.ssn.calc_goal_pos(
                    f_dsz=f_dsz,
                    map=global_map,
                    persons=persons,
                    group_center=center,
                    robot=robot_pose)
                goal = list(goal)
            else:
                return None
        else:
            return None
        rospy.loginfo("goal : {}".format(goal))
        return goal