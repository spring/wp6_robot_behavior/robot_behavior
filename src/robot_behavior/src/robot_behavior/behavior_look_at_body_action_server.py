#!/usr/bin/env python3
import rospy
import numpy as np
import actionlib
from spring_msgs.msg import LookAtEntityAction, LookAtEntityFeedback, LookAtEntityResult, LookAtEntity
from spring_msgs.msg import ControllerStatus


class LookAtBodyActionServer:
    def __init__(self, name):
        self._action_name = name
        self.hz = rospy.get_param('~hz', 20)
        self.success_ang_thresh = rospy.get_param('~success_ang_thresh', 0.2)

        self.controller_status_data = None

        self.current_time = None
        self.init_time = None
        self.head_joint_errors = [np.pi, np.pi]

        self._check_all_sensors_ready()

        self._publishers = []
        self._subscribers = []

        self._subscribers.append(rospy.Subscriber('/controller_status', ControllerStatus, callback=self._controller_status_callback, queue_size=1))

        self._look_at_body_pub  = rospy.Publisher('/action/look_at_body', LookAtEntity, queue_size=10)
        self._publishers.append(self._look_at_body_pub)

        rospy.on_shutdown(self.shutdown)

        self._a_server = actionlib.SimpleActionServer(self._action_name, LookAtEntityAction, execute_cb=self.execute_cb, auto_start=False)
        self._a_server.start()

        rospy.loginfo("{} Initialization Ended".format(self._action_name))


    def execute_cb(self, goal):
        # helper variables
        rate = rospy.Rate(self.hz)
        self.init_time = rospy.Time.now()
        self.current_time = (rospy.Time.now() - self.init_time).to_sec()

        self.publish_look_at_body(goal)
        self.head_joint_errors = [np.pi, np.pi]
        rospy.sleep(0.5)

        # publish info to the console for the user
        rospy.logdebug('{}: Look at body_id {}'.format(self._action_name, goal.id))
        rospy.logdebug('The target must be reached in %i seconds' % (goal.timeout))
        rospy.logdebug('Goal message : {}'.format(goal))

        # start executing the action
        while not rospy.is_shutdown():
            if self._a_server.is_preempt_requested():
                self._a_server.set_preempted()
                break

            self.current_time = (rospy.Time.now() - self.init_time).to_sec()

            # create messages that are used to publish feedback/result
            self._feedback = LookAtEntityFeedback()
            self._result = LookAtEntityResult()

            # publish the feedback
            self._publish_feedback(goal)
            rate.sleep()

            if len(self.head_joint_errors) > 0 and max(self.head_joint_errors) < self.success_ang_thresh:
                rospy.loginfo('%s: Succeeded' % self._action_name)
                self._a_server.set_succeeded(self._result)
                break

            if self.controller_status_data.status == ControllerStatus.IS_FAILURE:
                text = "Controller failure"
                rospy.loginfo('{}: Aborted. {}'.format(self._action_name, text))
                self._a_server.set_aborted(self._result, text=text)
                break

            if self.current_time > goal.timeout :
                text = "Timeout"
                rospy.loginfo('{}: Aborted. {}'.format(self._action_name, text))
                self._a_server.set_aborted(self._result)
                break

        self._result_msg(goal)

        # go to idle state
        goal.id = 'b-1'
        goal.continuous = False
        self.publish_look_at_body(goal)


    def publish_look_at_body(self, goal):
        look_at_body_msg = LookAtEntity()
        look_at_body_msg.id = goal.id
        look_at_body_msg.continuous = goal.continuous
        self._look_at_body_pub.publish(look_at_body_msg)


    def _result_msg(self, goal):
        self.current_time = (rospy.Time.now() - self.init_time).to_sec()
        self._create_action_msg(goal, self._result)
        self._result.final_time = self.current_time


    def _publish_feedback(self, goal):
        self._create_action_msg(goal, self._feedback)
        self._feedback.current_time = self.current_time
        self._a_server.publish_feedback(self._feedback)


    def _create_action_msg(self, goal, action_msg):
        if self.controller_status_data.head_joint_errors:
            self.head_joint_errors = self.controller_status_data.head_joint_errors
            action_msg.head_joint_errors = self.head_joint_errors
        else:
            action_msg.head_joint_errors = []


    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_controller_status_ready()
        rospy.logdebug("ALL SENSORS READY")


    def _check_controller_status_ready(self):
        self.controller_status_data = None
        rospy.logdebug("Waiting for /controller_status to be READY...")
        while self.controller_status_data is None and not rospy.is_shutdown():
            try:
                self.controller_status_data = rospy.wait_for_message("/controller_status", ControllerStatus, timeout=5.0)
                rospy.logdebug("Current /controller_status READY=>")

            except:
                rospy.logerr("Current /controller_status not ready yet, retrying for getting controller status")
        return self.controller_status_data


    def _controller_status_callback(self, data):
        self.controller_status_data = data


    def shutdown(self):
        rospy.loginfo("Stopping the %s " % self._action_name)
        self.close()
        rospy.loginfo("Killing the %s node" % self._action_name)


    def close(self):
        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()
        if self._publishers:
            for publisher in self._publishers:
                publisher.unregister()
