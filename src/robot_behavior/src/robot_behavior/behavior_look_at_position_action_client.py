#!/usr/bin/env python3
import rospy
import actionlib
from spring_msgs.msg import LookAtPositionAction, LookAtPositionGoal

class LookAtPositionActionClient:
    def __init__(self, name):
        self._action_name = name
        self.look_at_position = rospy.get_param('~look_at_position', '[0., 0., 0., 0.]')
        self.look_at_position = self.look_at_position.strip("[]").split(", ")
        self.look_at_position = list(map(float, self.look_at_position))

        self.timeout = rospy.get_param("~timeout")
        self.continuous = rospy.get_param('~continuous', False)

        rospy.on_shutdown(self.shutdown)

        # Creates the SimpleActionClient
        self._a_client = actionlib.SimpleActionClient(self._action_name, LookAtPositionAction)

        # Waits until the action server has started up and started listening for goals
        self._a_client.wait_for_server()
        self._a_client.cancel_all_goals()
        rospy.sleep(0.1)

        rospy.loginfo("{} Initialization Ended".format(self._action_name))


    def call_server(self):
        goal = LookAtPositionGoal()
        goal.look_at_position = self.look_at_position
        goal.timeout = self.timeout
        goal.continuous = self.continuous

        # Sends the goal to the action server
        self._a_client.send_goal(goal, feedback_cb=self.feedback_cb)

        # Waits for the server to finish performing the action
        self._a_client.wait_for_result()

        # Prints out the result of executing the action
        result = self._a_client.get_result()
        return result

    def feedback_cb(self, msg):
        rospy.loginfo('Feedback received: {}'.format(msg))


    def shutdown(self):
        rospy.loginfo("Stopping the %s " % self._action_name)
        self._a_client.cancel_all_goals()
        rospy.loginfo("Killing the %s node" % self._action_name)
