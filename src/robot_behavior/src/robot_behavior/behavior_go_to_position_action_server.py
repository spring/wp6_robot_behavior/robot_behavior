#!/usr/bin/env python3
import rospy
import numpy as np
import actionlib
from spring_msgs.msg import GoToPositionAction, GoToPositionFeedback, GoToPositionResult, GoToPosition
from spring_msgs.msg import ControllerStatus


class GoToPositionActionServer:
    def __init__(self, name):
        self._action_name = name
        self.hz = rospy.get_param('~hz', 20)
        self.success_dist_thresh = rospy.get_param('~success_dist_thresh', 0.3)
        self.success_ang_thresh = rospy.get_param('~success_ang_thresh', 0.2)

        self.controller_status_data = None

        self.current_time = None
        self.init_time = None
        self.dist_to_goto_target = np.inf
        self.ang_to_goto_target = np.pi

        self._check_all_sensors_ready()

        self._publishers = []
        self._subscribers = []

        self._subscribers.append(rospy.Subscriber('/controller_status', ControllerStatus, callback=self._controller_status_callback, queue_size=1))

        self._go_to_position_pub  = rospy.Publisher('/action/go_to_position', GoToPosition, queue_size=10)
        self._publishers.append(self._go_to_position_pub)

        rospy.on_shutdown(self.shutdown)        

        self._a_server = actionlib.SimpleActionServer(self._action_name, GoToPositionAction, execute_cb=self.execute_cb, auto_start=False)
        self._a_server.start()

        rospy.loginfo("{} Initialization Ended".format(self._action_name))


    def execute_cb(self, goal):
        # helper variables
        rate = rospy.Rate(self.hz)
        self.dist_to_goto_target = np.inf
        self.ang_to_goto_target = np.pi
        self.init_time = rospy.Time.now()
        self.current_time = (rospy.Time.now() - self.init_time).to_sec()

        self.publish_go_to_position(goal)
        rospy.sleep(0.5)

        # publish info to the console for the user
        rospy.logdebug('{}: Begins the navigation planning to position {} with orientation {}'.format(self._action_name, goal.go_to_target_pose[:2], goal.go_to_target_pose[2]))
        rospy.logdebug('The target must be reached in %i seconds' % (goal.timeout))
        rospy.logdebug('Goal message : {}'.format(goal))

        # start executing the action
        while not rospy.is_shutdown():
            if self._a_server.is_preempt_requested():
                self._a_server.set_preempted()
                break

            self.current_time = (rospy.Time.now() - self.init_time).to_sec()

            # create messages that are used to publish feedback/result
            self._feedback = GoToPositionFeedback()
            self._result = GoToPositionResult()

            # publish the feedback
            self._publish_feedback(goal)
            rate.sleep()

            if self.dist_to_goto_target < self.success_dist_thresh and self.ang_to_goto_target < self.success_ang_thresh:
                rospy.loginfo('%s: Succeeded' % self._action_name)
                self._a_server.set_succeeded(self._result)
                break

            if self.controller_status_data.status == ControllerStatus.IS_FAILURE:
                text = "Controller failure"
                rospy.loginfo('{}: Aborted. {}'.format(self._action_name, text))
                self._a_server.set_aborted(self._result, text=text)
                break

            if self.current_time > goal.timeout :
                text = "Timeout"
                rospy.loginfo('{}: Aborted. {}'.format(self._action_name, text))
                self._a_server.set_aborted(self._result)
                break

        self._result_msg(goal)

        # go to idle state
        goal.go_to_target_pose = np.zeros_like(goal.go_to_target_pose)
        goal.go_to_target_pose[-1] = -1
        goal.continuous = False
        self.publish_go_to_position(goal)


    def publish_go_to_position(self, goal):
        go_to_position_msg = GoToPosition()
        go_to_position_msg.go_to_target_pose = goal.go_to_target_pose
        go_to_position_msg.continuous = goal.continuous
        self._go_to_position_pub.publish(go_to_position_msg)


    def _result_msg(self, goal):
        self.current_time = (rospy.Time.now() - self.init_time).to_sec()
        self._create_action_msg(goal, self._result)
        self._result.final_time = self.current_time


    def _publish_feedback(self, goal):
        self._create_action_msg(goal, self._feedback)
        self._feedback.current_time = self.current_time
        self._a_server.publish_feedback(self._feedback)


    def _create_action_msg(self, goal, action_msg):
        if self.controller_status_data.dist_to_goto_target:
            self.dist_to_goto_target = self.controller_status_data.dist_to_goto_target[-1]
            self.ang_to_goto_target = self.controller_status_data.dist_to_goto_target[-2]
            action_msg.dist_to_goto_target = self.controller_status_data.dist_to_goto_target
        else:
            action_msg.dist_to_goto_target = []


    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_controller_status_ready()
        rospy.logdebug("ALL SENSORS READY")


    def _check_controller_status_ready(self):
        self.controller_status_data = None
        rospy.logdebug("Waiting for /controller_status to be READY...")
        while self.controller_status_data is None and not rospy.is_shutdown():
            try:
                self.controller_status_data = rospy.wait_for_message("/controller_status", ControllerStatus, timeout=5.0)
                rospy.logdebug("Current /controller_status READY=>")

            except:
                rospy.logerr("Current /controller_status not ready yet, retrying for getting controller status")
        return self.controller_status_data


    def _controller_status_callback(self, data):
        self.controller_status_data = data


    def shutdown(self):
        rospy.loginfo("Stopping the %s " % self._action_name)
        self.close()
        rospy.loginfo("Killing the %s node" % self._action_name)


    def close(self):
        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()
        if self._publishers:
            for publisher in self._publishers:
                publisher.unregister()
