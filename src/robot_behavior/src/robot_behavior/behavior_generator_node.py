#!/usr/bin/env python3
import numpy as np
from collections import namedtuple

import pkg_resources
import tf
import os
import yaml
import cv2
from multiprocessing import Lock

from social_mpc.controller import RobotController
from social_mpc.config.config import RobotConfig
from social_mpc.utils import local_to_global, global_to_local
from social_mpc.ssn_model.social_spaces import SocialSpaces, Group

import rospy
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3, PoseArray, Transform
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64
from nav_msgs.msg import Odometry, OccupancyGrid, MapMetaData
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue

from robot_behavior.utils import constraint_angle, meter_to_px
from spring_msgs.msg import TrackedPersons2d, ControllerStatus, GoToPosition, GoToEntity, LookAtEntity, LookAtPosition
from robot_behavior.ros_4_hri_interface import ROS4HRIInterface  


class BehaviorGenerator:
    def __init__(self):
        os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'
        mpc_config_name = rospy.get_param('~mpc_config_name', 'None')
        robot_config_name = rospy.get_param('~robot_config_name', 'None')
        mpc_config_path = None if mpc_config_name == 'None' else mpc_config_name
        robot_config_path = None if robot_config_name == 'None' else robot_config_name
        self.mpc_config_path = mpc_config_path
        self.robot_config_path = robot_config_path
        self.max_humans_world = rospy.get_param('max_humans_world', 20)
        self.max_groups_world = rospy.get_param('max_groups_world', 10)
        self.namespace = rospy.get_param('namespace', 'behavior_generator')
        self.namespace_slam = rospy.get_param('~namespace_slam', '/rtabmap')
        self.map_frame = rospy.get_param('~map_frame', 'map')
        self.robot_frame = rospy.get_param('~robot_frame', 'base_footprint')
        self.global_occupancy_map_topic = rospy.get_param('~global_occupancy_map_topic', '/slam/global_occupancy_map')
        self.local_occupancy_map_topic = rospy.get_param('~local_occupancy_map_topic', '/slam/local_occupancy_map')
        self.diag_timer_rate = rospy.get_param('~diag_timer_rate', 10)
        self.target_id_absent_treshold = rospy.get_param('~target_id_absent_treshold', 3)

        self.controller = RobotController(config_filename=self.mpc_config_path)

        cmd = "rostopic pub -1  /interaction_profile/set/cancel actionlib_msgs/GoalID \"{}\""
        nodes = os.popen("rosnode list").readlines()
        for i in range(len(nodes)):
            nodes[i] = nodes[i].replace("\n","")
        if '/pal_head_manager' in nodes:
            os.system("rosnode kill " + '/pal_head_manager')  # '/pal_webcommander'
        os.system(cmd)  # disable alive mode

        cmd_pub = "rostopic pub -1 /pause_navigation std_msgs/Bool 'data: false'"
        os.system(cmd_pub)  # disable pause navigation mode

        self.joint_states_data = None
        self.global_map_data = None
        self.local_map_data = None
        self.tracked_persons_2d_data = None
        self.go_to_position_data = None
        self.go_to_body_data = None
        self.go_to_person_data = None
        self.go_to_group_data = None
        self.look_at_position_data = None
        self.look_at_body_data = None
        self.look_at_person_data = None
        self.look_at_group_data = None
        self.rtabmap_ready = False
        self.hri_data = None

        self.look_at_group_target = None
        self.look_at_body_target = None
        self.look_at_person_target = None
        self.look_at_position_target = None
        self.look_at_done = False

        self.go_to_group_target = None
        self.go_to_body_target = None
        self.go_to_person_target = None
        self.go_to_position_target = None
        self.go_to_done = False

        # To get indexes of the joints
        self.get_indexes = False
        self.pan_ind = None
        self.tilt_ind = None
        self.current_pan = None
        self.current_tilt = None
        self.head_goal = False
        self.base_goal = False
        self.pan_angle = 0.
        self.tilt_angle = 0.
        self.lin_vel = 0.
        self.ang_vel = 0.
        
        self.human_id_default = -1
        self.group_id_default = -1
        self.x_pan_target = 0.
        self.y_pan_target = 0.
        self.wrong_target = False
        self.x_final = 0.
        self.y_final = 0.
        self.th_final = 0.
        self.x_wp = 0.
        self.y_wp = 0.
        self.th_wp = 0.
        self.head_joint_errors = False
        self.x_human_features = 0.
        self.y_human_features = 0.
        self.th_human_features = 0.
        self.x_robot_pose = 0.
        self.y_robot_pose = 0.
        self.yaw_robot_pose = 0.

        self.target_id_absent_count = 0

        # continuous flags
        self.is_continuous_look_at = False
        self.continuous_look_at_done = False
        self.is_continuous_go_to = False
        self.continuous_go_to_done = False

        self.robot_config = None

        self.action_idx = None
        self.actions = None
        self.controller_status_msg = None

        self.timestamp_tracking = None
        self.timestamp_tracking_latest = None
        self.lock = Lock()
        self.lock_controller = Lock()
        self.ssn = None

        self._subscribers = []
        self._publishers = []
        self._timers = []

        self.read_robot_config(self.robot_config_path)
        self.init_state()

        self.init()


    def init(self):
        self.i = 0
        self.ros_4_hri_interface = ROS4HRIInterface()
        self.tf_broadcaster = tf.TransformBroadcaster()
        self.tf_listener = tf.TransformListener()

        self._check_all_sensors_ready()

        # Start all the ROS related Subscribers and publishers
        self._subscribers.append(rospy.Subscriber('/joint_states', JointState, callback=self._joint_states_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber('/tracked_persons_2d', TrackedPersons2d, callback=self._tracked_persons_2d_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.global_occupancy_map_topic, OccupancyGrid, callback=self._global_map_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.local_occupancy_map_topic, OccupancyGrid, callback=self._local_map_callback, queue_size=1))

        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/go_to_position', GoToPosition, callback=self._go_to_position_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/go_to_body', GoToEntity, callback=self._go_to_body_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/go_to_person', GoToEntity, callback=self._go_to_person_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/go_to_group', GoToEntity, callback=self._go_to_group_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/look_at_position', LookAtPosition, callback=self._look_at_position_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/look_at_body', LookAtEntity, callback=self._look_at_body_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/look_at_person', LookAtEntity, callback=self._look_at_person_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.namespace + '/action/look_at_group', LookAtEntity, callback=self._look_at_group_callback, queue_size=1))

        self._timers.append(rospy.Timer(rospy.Duration(self.controller.controller_config.h), callback=self._pub_vel_callback))
        self._timers.append(rospy.Timer(rospy.Duration(1/self.diag_timer_rate), callback=self._diagnostics_callback))
        self.rate = rospy.Rate(int(1/self.controller.controller_config.h))
        
        self._cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        self._publishers.append(self._cmd_vel_pub)
        self._pan_vel_pub = rospy.Publisher('/head_controller/command', JointTrajectory, queue_size=1)
        self._publishers.append(self._pan_vel_pub)
        self._controller_status_pub = rospy.Publisher(self.namespace + '/controller_status', ControllerStatus, queue_size=1)
        self._publishers.append(self._controller_status_pub)
        self._diagnostics_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=1)
        self._publishers.append(self._diagnostics_pub)
        self._local_social_cost_map_pub = rospy.Publisher(self.namespace_slam + '/local_social_cost_map', OccupancyGrid, queue_size=1, latch=True)
        self._publishers.append(self._local_social_cost_map_pub)
        
        rospy.loginfo("Initializing the BehaviorGenerator")

        # Register handler to be called when rospy process begins shutdown
        # rospy.on_shutdown(self.shutdown)  # calling shutdown several times because of sub processes

        self._check_publishers_connection()

        if self.ssn is None:
            local_map_size = [
                [self.local_map_data.info.origin.position.x, self.local_map_data.info.origin.position.x + self.local_map_data.info.width*self.local_map_data.info.resolution],
                [self.local_map_data.info.origin.position.y, self.local_map_data.info.origin.position.y + self.local_map_data.info.height*self.local_map_data.info.resolution]]
            self.ssn = SocialSpaces(
                    bbox=local_map_size,
                    map_shape=(self.local_map_data.info.height, self.local_map_data.info.width))

        self.get_robot_pose()
        self.recreate_state()
        self.controller.controller_config.goto_target_id = -1
        self.controller.controller_config.human_target_id = -1
        self.controller.controller_config.pan_target_id = -1
        self.controller.controller_config.goto_target_pose = np.zeros_like(self.controller.controller_config.goto_target_pose)
        self.controller.step(self.state)
        self.controller.reg_parameter[-1] = 100000
        self.controller.mpc.step(state=self.state.joint_angles,
                                         actions=self.controller.actions + self.controller.initial_action,
                                         weights=self.controller.weights,
                                         reg_parameter=self.controller.reg_parameter,
                                         loss_coef=self.controller.loss_coef,
                                         loss_rad=self.controller.loss_rad,
                                         cost_map=self.controller.local_map[:, :, :-1],
                                         goto_goal=self.controller.goto_goal,
                                         pan_goal=self.controller.pan_goal,
                                         human_features=None) #self.controller.human_features)

        self.action_idx = 0
        self.actions = np.copy(self.controller.initial_action)

        if not self.controller.is_init:
            rospy.signal_shutdown("BehaviorGenerator shutting down.")

        rospy.loginfo("Arguements passed : ")
        rospy.loginfo("Config path : {}".format(self.mpc_config_path))
        rospy.loginfo("BehaviorGenerator Initialization Ended")


    def read_robot_config(self, filename=None):
        if filename is None:
            filename = pkg_resources.resource_filename(
                'sim2d', 'config/robot.yaml')
            rospy.logdebug("No filename provided for the robot configuration, basic robot config loaded")
        elif os.path.isfile(filename):
            rospy.logdebug("Desired robot config loaded")
        else:
            filename = pkg_resources.resource_filename(
                'sim2d', 'config/robot.yaml')
            rospy.logdebug("Desired filename for the robot configuration does not exist, basic robot config loaded")
        config = yaml.load(open(filename), Loader=yaml.FullLoader)
        self.robot_config = RobotConfig(config)


    def run(self):
        init_t = rospy.Time.now()
        while not rospy.is_shutdown():
            self.step(init_t)

    def step(self, init_time):
        loop_t = rospy.Time.now()
        self.get_robot_pose()
        with self.lock:
            self.recreate_state()
        rospy.logdebug('recreate_state time : {}'.format((rospy.Time.now() - loop_t).to_sec()))
        self.controller.step(self.state)
        rospy.logdebug('MPC time : {}'.format((rospy.Time.now() - loop_t).to_sec()))
        self.publish_controller_status()
        self.publish_tf_go_to_goals()
        self.publish_tf_look_at_goals()
        self.publish_tf_human_goal()

        time_now = rospy.Time.now()
        rospy.logdebug('BehaviorGenerator time : {}, BehaviorGenerator loop time : {}'.format((time_now - init_time).to_sec(), (time_now - loop_t).to_sec()))
        rospy.logdebug('Step time : {}'.format((time_now - loop_t).to_sec()))

        self.rate.sleep()
        self.i += 1


    def init_state(self):
        self.state = RobotState()
        self.list_attr = ["robot_pose",
                          "robot_velocity",
                          "joint_angles",
                          "joint_velocities",
                          "local_map",
                          "global_map",
                          "groups_pose",
                          "humans_pose",
                          "humans_velocity",
                          "humans_visible",
                          "groups_visible",
                          "groups_id",
                          "humans_group_id",
                          "humans_id",
                          "config",
                          "robot_config"]
        for attr in self.list_attr:
            self.state.__setattr__(attr, None)

        self.Config = namedtuple('Config', ['global_map_size',
                                            'global_map_scale',
                                            'global_map_width',
                                            'global_map_height',
                                            'local_map_size',
                                            'local_map_scale',
                                            'local_map_width',
                                            'local_map_height'])

        self.RobotConfig = namedtuple('RobotConfig', ['has_pan',
                                                      'has_tilt',
                                                      'min_pan_angle',
                                                      'max_pan_angle',
                                                      'base_footprint_radius'])
        self.state.robot_config = self.RobotConfig(self.robot_config.has_pan,
                                                   self.robot_config.has_tilt,
                                                   self.robot_config.min_pan_angle,
                                                   self.robot_config.max_pan_angle,
                                                   self.robot_config.base_footprint_radius)

        self.reset_state()

    def reset_state(self):
        nb_humans = self.max_humans_world
        self.state.humans_id = np.ones(nb_humans) * self.human_id_default
        self.state.humans_group_id = np.ones(nb_humans) * self.group_id_default
        self.state.groups_id = np.ones(self.max_groups_world) * self.group_id_default
        self.state.humans_visible = np.ones(nb_humans)
        self.state.groups_visible = np.ones(self.max_groups_world)
        self.state.humans_pose = np.zeros((nb_humans, 3))  # x, y, angle
        self.state.groups_pose = np.zeros((self.max_groups_world, 2))  # x, y
        self.state.humans_velocity = np.zeros((nb_humans, 2))
        self.state.robot_pose = np.zeros(3)
        self.state.robot_velocity = np.zeros(2)
        if self.state.robot_config.has_pan and self.state.robot_config.has_tilt:
            self.state.joint_angles = np.zeros(2)
            self.state.joint_velocities = np.zeros(2)
        elif not self.state.robot_config.has_pan and not self.state.robot_config.has_tilt:
            self.state.joint_angles = None
            self.state.joint_velocities = None
        else:
            self.state.joint_angles = np.zeros(1)
            self.state.joint_velocities = np.zeros(1)


    def publish_controller_status(self):
        controller_status_msg = ControllerStatus()
        controller_status_msg.goto_target_id = self.controller.controller_config.goto_target_id
        controller_status_msg.human_target_id = self.controller.controller_config.human_target_id
        controller_status_msg.pan_target_id = self.controller.controller_config.pan_target_id
        if self.look_at_body_target:
            controller_status_msg.pan_target_id = self.look_at_body_target
        if self.look_at_group_target:
            controller_status_msg.pan_target_id = self.look_at_group_target
        if self.look_at_person_target:
            controller_status_msg.pan_target_id = self.look_at_person_target

        controller_status_msg.is_continuous_look_at = self.is_continuous_look_at
        controller_status_msg.continuous_look_at_done = self.continuous_look_at_done
        controller_status_msg.is_continuous_go_to = self.is_continuous_go_to        
        controller_status_msg.continuous_go_to_done = self.continuous_go_to_done

        self.x_human_features, self.y_human_features, self.th_human_features = self.controller.human_features[:3]
        self.x_final = self.controller.shared_target[2]
        self.y_final = self.controller.shared_target[3]
        self.th_final = self.controller.shared_target[4]
        self.x_wp = self.controller.goto_goal[0]
        self.y_wp = self.controller.goto_goal[1]
        self.th_wp = self.controller.goto_goal[2]

        if controller_status_msg.goto_target_id != -1 or self.controller.controller_config.goto_target_pose[-1] == 1:
            dist_to_goto_target = np.linalg.norm(np.array([self.x_robot_pose - self.x_final, self.y_robot_pose - self.y_final]))
            controller_status_msg.dist_to_goto_target = [np.abs(self.x_robot_pose - self.x_final),
                                              np.abs(self.y_robot_pose - self.y_final),
                                              np.abs(constraint_angle(self.yaw_robot_pose - self.th_final)),
                                              dist_to_goto_target]

        if controller_status_msg.human_target_id != -1:
            dist_to_human_target = np.linalg.norm(np.array([self.x_human_features, self.y_human_features]))
            controller_status_msg.dist_to_human_target = [np.abs(self.x_human_features),
                                               np.abs(self.y_human_features),
                                               np.abs(constraint_angle(self.th_human_features)),
                                               dist_to_human_target]

        if controller_status_msg.pan_target_id != -1 or self.controller.pan_target_pose is not None or self.head_goal:
            head_joint_errors = []
            if self.head_joint_errors:
                if self.pan_ind is not None:
                    head_joint_errors.append(np.abs(constraint_angle(np.arctan2(self.y_pan_target - self.y_robot_pose, self.x_pan_target - self.x_robot_pose) - constraint_angle(self.yaw_robot_pose + self.current_pan))))
                if self.tilt_ind is not None:
                    head_joint_errors.append(0.)
                controller_status_msg.head_joint_errors = head_joint_errors

        if controller_status_msg.pan_target_id == -1 and self.controller.pan_target_pose is None and controller_status_msg.human_target_id == -1 and controller_status_msg.goto_target_id == -1 and self.controller.controller_config.goto_target_pose[-1] == 0:
            controller_status_msg.status = ControllerStatus.IDLE
        if controller_status_msg.pan_target_id != -1 or self.controller.pan_target_pose is not None or controller_status_msg.human_target_id != -1 or controller_status_msg.goto_target_id != -1 or self.controller.controller_config.goto_target_pose[-1] == 1:
            controller_status_msg.status = ControllerStatus.IS_RUNNING
        if (self.controller.is_failure or self.wrong_target) and not(self.go_to_done or self.look_at_done):
            rospy.logwarn('FAILURE from controller {}, from target {}'.format(self.controller.is_failure, self.wrong_target))
            controller_status_msg.status = ControllerStatus.IS_FAILURE
        self.controller_status_msg = controller_status_msg
        self._controller_status_pub.publish(self.controller_status_msg)


    def publish_tf_human_goal(self):
        if self.controller.controller_config.human_target_id != -1:
            current_time = rospy.Time.now()
            quat = tf.transformations.quaternion_from_euler(0, 0, self.th_human_features)
            self.tf_broadcaster.sendTransform(
             (self.x_human_features, self.y_human_features, 0),
             quat,
             current_time,
             "human_goal",
             self.map_frame
            )


    def publish_tf_go_to_goals(self):
        if self.base_goal:
            current_time = rospy.Time.now()
            quat_final = tf.transformations.quaternion_from_euler(0, 0, self.th_final)
            quat_wp = tf.transformations.quaternion_from_euler(0, 0, self.th_wp)

            self.tf_broadcaster.sendTransform(
             (self.x_final, self.y_final, 0),
             quat_final,
             current_time,
             "final goal",
             self.map_frame
            )

            self.tf_broadcaster.sendTransform(
             (self.x_wp, self.y_wp, 0),
             quat_wp,
             current_time,
             "intermediate waypoint goal",
             self.robot_frame
            )


    def publish_tf_look_at_goals(self):
        if self.head_goal:
            current_time = rospy.Time.now()
            quat_pan_goal = tf.transformations.quaternion_from_euler(0, 0, self.yaw_robot_pose)
            self.tf_broadcaster.sendTransform(
             (self.x_pan_target, self.y_pan_target, 0),
             quat_pan_goal,
             current_time,
             "pan goal",
             self.map_frame
            )


    def publish_head_cmd(self, pan, tilt):
        pan_angle, tilt_angle = self._check_joint_pos_limits(pan, tilt)
        
        head_msg = JointTrajectory()
        p = JointTrajectoryPoint()
        p.positions = [None] * 2
        p.positions[0] = pan_angle
        p.positions[1] = tilt_angle
        # head_msg.header.frame_id = "head"
        # head_msg.header.stamp = rospy.Time.now()
        head_msg.joint_names = [None]*2
        head_msg.joint_names[0] = "head_1_joint"  # pan
        head_msg.joint_names[1] = "head_2_joint"  # tilt
        head_msg.points = [None]
        head_msg.points[0] = p
        head_msg.points[0].time_from_start = rospy.Duration.from_sec(self.controller.controller_config.h)
        self._pan_vel_pub.publish(head_msg)
        rospy.loginfo('pan_angle goal : {}  tilt angle goal : {}'.format(pan_angle, tilt_angle))


    def publish_vel(self, action):
        pan_vel, ang, lin = action[0], action[1], action[2]
        self.lin_vel, self.ang_vel = self._check_joint_vel_limits(lin, ang)
        twist_msg = Twist(Vector3(self.lin_vel, 0, 0), Vector3(0, 0, self.ang_vel))
        self._cmd_vel_pub.publish(twist_msg)

        """
        pan_angle = self.current_pan
        pan_angle += self.controller.controller_config.h*pan_vel
        tilt_angle = 0.  #self.current_tilt
        pan_angle, tilt_angle = self._check_joint_pos_limits(pan_angle, tilt_angle)

        head_msg = JointTrajectory()
        p = JointTrajectoryPoint()
        p.positions = [None] * 2
        p.positions[0] = pan_angle
        p.positions[1] = tilt_angle
        # head_msg.header.frame_id = "head"
        # head_msg.header.stamp = rospy.Time.now()
        head_msg.joint_names = [None]*2
        head_msg.joint_names[0] = "head_1_joint"  # pan
        head_msg.joint_names[1] = "head_2_joint"  # tilt
        head_msg.points = [None]
        head_msg.points[0] = p
        head_msg.points[0].time_from_start = rospy.Duration.from_sec(self.controller.controller_config.h)
        self._pan_vel_pub.publish(head_msg)
        """
        rospy.loginfo("vel to robot : {}, {}, {}".format(lin, ang, pan_vel))


    def get_robot_pose(self):
        # try:
        #     map_2_robot = self.tf_listener.lookupTransform(self.map_frame, self.robot_frame, self.timestamp_tracking_latest)
        # except:
        try:
            map_2_robot = self.tf_listener.lookupTransform(self.map_frame, self.robot_frame, rospy.Time(0))
            rospy.logwarn("Could not get transformation between {} and {} at correct time. Using latest available.".format(self.map_frame, self.robot_frame))
        except:
            rospy.logerr("Could not get transformation between {} and {}.".format(self.map_frame, self.robot_frame))
            self.x_robot_pose, self.y_robot_pose, self.yaw_robot_pose = 0., 0., 0.
            return None
        self.x_robot_pose, self.y_robot_pose, _ = map_2_robot[0]
        (_, _, yaw) = tf.transformations.euler_from_quaternion([map_2_robot[1][0],
                                                                map_2_robot[1][1],
                                                                map_2_robot[1][2],
                                                                map_2_robot[1][3]])
        self.yaw_robot_pose = float(constraint_angle(yaw))


    def recreate_state(self):
        self.reset_state()
        self.timestamp_tracking_latest = self.timestamp_tracking

        self.current_pan = self.joint_states_data.position[self.pan_ind]
        self.current_tilt = self.joint_states_data.position[self.tilt_ind]
        self.state.robot_pose = np.array([self.x_robot_pose, self.y_robot_pose, self.yaw_robot_pose])
        # self.state.robot_velocity = np.array([lin, ang])

        self.state.joint_angles = np.array([self.joint_states_data.position[self.pan_ind]])
        self.state.joint_velocities = np.array([self.joint_states_data.velocity[self.pan_ind]])
        
        for human_id, human_data in self.hri_data[0].items():
            human_id = int(human_id[1:])
            humans_id_list = self.state.humans_id.tolist()
            if human_id in humans_id_list:
                index = humans_id_list.index(human_id)
            elif self.human_id_default in humans_id_list:
                index = humans_id_list.index(self.human_id_default)
            else:
                rospy.logerr("No more space in state.humans_ arrays. Number max is {}. It is fixed because of the use of SharedMemory multiprocessing".format(self.max_humans_world))
            self.state.humans_id[index] = human_id
            self.state.humans_visible[index] = 0.  # how to get it ?
            if len(human_data) >= 3:
                self.state.humans_group_id[index] = int(human_data[2][3:])  # 0: transform, 1:person_id, 2:group_id if group

            human_pose = human_data[0].transform.translation
            human_quat = human_data[0].transform.rotation
            x_pos, y_pos = human_pose.x, human_pose.y
            (_, _, yaw) = tf.transformations.euler_from_quaternion([
                human_quat.x,
                human_quat.y,
                human_quat.z,
                human_quat.w])

            lin = 0.  # how to get it ?
            ang = 0.  # how to get it ?

            self.state.humans_pose[index, :] = [x_pos, y_pos, constraint_angle(yaw)]
            self.state.humans_velocity[index, :] = [lin, ang]

            if self.look_at_body_target is not None:
                if (self.controller.pan_goal[-1] == 1 or self.head_goal) and self.look_at_body_target == human_id:
                    self.head_joint_errors = True
                    self.x_pan_target = x_pos
                    self.y_pan_target = y_pos
            
            if self.look_at_person_target is not None:
                if (self.controller.pan_goal[-1] == 1 or self.head_goal) and self.look_at_person_target == human_id:
                    self.head_joint_errors = True
                    self.x_pan_target = x_pos
                    self.y_pan_target = y_pos

        rospy.logdebug('humans_id : {}'.format(self.state.humans_id.tolist()))

        for group_id, group_data in self.hri_data[1].items():
            group_id  = int(group_id[3:])
            groups_id_list = self.state.groups_id.tolist()
            if group_id in groups_id_list:
                index = groups_id_list.index(group_id)
            elif self.group_id_default in groups_id_list:
                index = groups_id_list.index(self.group_id_default)
            else:
                rospy.logerr("No more space in state.groups_ arrays. Number max is {}. It is fixed because of the use of SharedMemory multiprocessing".format(self.max_groups_world))
            self.state.groups_id[index] = group_id
            self.state.groups_visible[index] = 0.  # how to get it ?

            group_pose = group_data[0].transform.translation
            x_pos, y_pos = group_pose.x, group_pose.y
            self.state.groups_pose[index, :] = [x_pos, y_pos]

            if self.look_at_group_target is not None:
                if (self.controller.pan_goal[-1] == 1 or self.head_goal) and self.look_at_group_target == group_id:
                    self.head_joint_errors = True
                    self.x_pan_target = x_pos
                    self.y_pan_target = y_pos

        rospy.logdebug('groups_id ; {}'.format(self.state.groups_id.tolist()))

        # for person_id, person_data in self.hri_data[2].items():
        #     print('person_id', person_id, 'person_data', person_data)
        # for body_id, body_data in self.hri_data[3].items():
        #    print('body_id', body_id, 'body_data', body_data)

        if self.look_at_position_target is not None:
            if self.controller.pan_goal[-1] == 1 or self.head_goal:
                self.head_joint_errors = True
                if self.look_at_position_target[-2]:
                    (self.x_pan_target, self.y_pan_target) = local_to_global(np.asarray([self.x_robot_pose, self.y_robot_pose, self.yaw_robot_pose]), self.look_at_position_target[:2])  # local to global map target
                else:
                    self.x_pan_target = self.look_at_position_target[0]
                    self.y_pan_target = self.look_at_position_target[1]
        
        if self.head_goal:
            if self.look_at_body_target is not None:
                if float(self.look_at_body_target) not in self.state.humans_id.tolist():
                    self.reset_look_at_target()
                    self.wrong_target = True
                    if self.is_continuous_look_at:
                        self.continuous_look_at_done = True
                    rospy.logwarn('reset head_goal look_at_body_target')
            if self.look_at_person_target is not None:
                if float(self.look_at_person_target) not in self.state.humans_id.tolist():
                    self.reset_look_at_target()
                    self.wrong_target = True
                    if self.is_continuous_look_at:
                        self.continuous_look_at_done = True
                    rospy.logwarn('reset head_goal look_at_person_target')
            if self.look_at_group_target is not None:
                if float(self.look_at_group_target) not in self.state.groups_id.tolist():
                    self.reset_look_at_target()
                    self.wrong_target = True
                    if self.is_continuous_look_at:
                        self.continuous_look_at_done = True
                    rospy.logwarn('reset head_goal look_at_group_target')
        
        if self.base_goal:
            if self.go_to_body_target is not None:
                if float(self.go_to_body_target) not in self.state.humans_id.tolist():
                    if self.target_id_absent_count > self.target_id_absent_treshold:
                        self.reset_go_to_target()
                        self.wrong_target = True
                        if self.is_continuous_go_to:
                            self.continuous_go_to_done = True
                        rospy.logwarn('reset base_goal go_to_body_target')
                    else:
                        self.target_id_absent_count += 1
                else:
                    self.target_id_absent_count = 0
            if self.go_to_person_target is not None:
                if float(self.go_to_person_target) not in self.state.humans_id.tolist():
                    if self.target_id_absent_count > self.target_id_absent_treshold:
                        self.reset_go_to_target()
                        self.wrong_target = True
                        if self.is_continuous_go_to:
                            self.continuous_go_to_done = True
                        rospy.logwarn('reset base_goal go_to_person_target')
                    else:
                        self.target_id_absent_count += 1
                else:
                    self.target_id_absent_count = 0
            if self.go_to_group_target is not None:
                if float(self.go_to_group_target) not in self.state.groups_id.tolist():
                    if self.target_id_absent_count > self.target_id_absent_treshold:
                        self.reset_go_to_target()
                        self.wrong_target = True
                        if self.is_continuous_go_to:
                            self.continuous_go_to_done = True
                        rospy.logwarn('reset base_goal go_to_group_target')
                    else:
                        self.target_id_absent_count += 1
                else:
                    self.target_id_absent_count = 0

        self.state.config = self.Config(self.global_map_size,
                                        round(1/self.global_map_resolution),
                                        self.global_map_width,
                                        self.global_map_height,
                                        self.local_map_size,
                                        round(1/self.local_map_resolution),
                                        self.local_map_width,
                                        self.local_map_height)

        self.state.global_map = self.global_map_static
        local_map = np.zeros((*self.local_static_map.shape, 3))
        local_map[:, :, 0] = self.local_static_map
        local_map[:, :, 1] = self.get_social_cost_map(data=self.local_map_data)
        self.state.local_map = local_map


    def _check_joint_vel_limits(self, lin_vel, ang_vel):
        if lin_vel > self.controller.controller_config.u_ub[-1]:
            rospy.logwarn("The linear velocity is greater than the maximum allowed. The maximum velocity is set.")
            lin_vel = self.controller.controller_config.u_ub[-1]
        if lin_vel < self.controller.controller_config.u_lb[-1]:
            rospy.logwarn("The linear velocity is lower than the minimum allowed. The minimum velocity is set.")
            lin_vel = self.controller.controller_config.u_lb[-1]
        if ang_vel > self.controller.controller_config.u_ub[-2]:
            rospy.logwarn("The angular velocity is greater than the maximum allowed. The maximum velocity is set.")
            ang_vel = self.controller.controller_config.u_ub[-2]
        if ang_vel < self.controller.controller_config.u_lb[-2]:
            rospy.logwarn("The angular velocity is lower than the minimum allowed. The minimum velocity is set.")
            ang_vel = self.controller.controller_config.u_lb[-2]
        return lin_vel, ang_vel


    def _check_joint_pos_limits(self, p, t):
        if p < self.robot_config.min_pan_angle:
            p = self.robot_config.min_pan_angle
            rospy.logwarn("The pan angle desired is lower than the miminum allowed. The minimum oan angle is set.")
        if p > self.robot_config.max_pan_angle:
            p = self.robot_config.max_pan_angle
            rospy.logwarn("The pan angle desired is greater than the maximum allowed. The maximum oan angle is set.")
        if t < self.robot_config.min_tilt_angle:
            t = self.robot_config.min_tilt_angle
            rospy.logwarn("The tilt angle desired is lower than the miminum allowed. The minimum oan angle is set.")
        if t > self.robot_config.max_tilt_angle:
            t = self.robot_config.max_tilt_angle
            rospy.logwarn("The tilt angle desired is higher than the maximum allowed. The maximum oan angle is set.")
        return p, t


    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        # self._check_rtabmap_ready()
        self._check_joint_states_ready()
        self._check_tracked_persons_2d_ready()
        self._check_global_map_ready()
        self._check_local_map_ready()
        rospy.logdebug("ALL SENSORS READY")


    def _check_global_map_ready(self):
        self.global_map_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.global_occupancy_map_topic))
        while self.global_map_data is None and not rospy.is_shutdown():
            try:
                self.global_map_data = rospy.wait_for_message(self.global_occupancy_map_topic, OccupancyGrid, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.global_occupancy_map_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for getting global map".format(self.global_occupancy_map_topic))
        return self.global_map_data


    def _check_local_map_ready(self):
        self.local_map_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.local_occupancy_map_topic))
        while self.local_map_data is None and not rospy.is_shutdown():
            try:
                self.local_map_data = rospy.wait_for_message(self.local_occupancy_map_topic, OccupancyGrid, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.local_occupancy_map_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for getting local map".format(self.local_occupancy_map_topic))
        return self.local_map_data


    def _check_tracked_persons_2d_ready(self):
        self.tracked_persons_2d_data = None
        rospy.logdebug("Waiting for /tracked_persons_2d to be READY...")
        while self.tracked_persons_2d_data is None and not rospy.is_shutdown():
            try:
                self.tracked_persons_2d_data = rospy.wait_for_message("/tracked_persons_2d", TrackedPersons2d, timeout=5.0)
                rospy.logdebug("Current /tracked_persons_2d READY=>")

            except:
                rospy.logerr("Current /tracked_persons_2d not ready yet, retrying for getting tracked persons 2d")
        return self.tracked_persons_2d_data


    def _check_joint_states_ready(self):
        self.joint_states_data = None
        rospy.logdebug("Waiting for /joint_states to be READY...")
        while self.joint_states_data is None and not rospy.is_shutdown():
            try:
                self.joint_states_data = rospy.wait_for_message("/joint_states", JointState, timeout=5.0)
                rospy.logdebug("Current /joint_states READY=>")

            except:
                rospy.logerr("Current /joint_states not ready yet, retrying for getting joint states")
        if not self.get_indexes:
            # joint_states : head_1_joint, head_2_joint, wheel_left_joint, wheel_right_joint, caster_left_joint, caster_right_joint
            for index, name in enumerate(self.joint_states_data.name):
                if name == "head_1_joint":
                    self.pan_ind = index
                if name == "head_2_joint":
                    self.tilt_ind = index
            self.get_indexes = True

        return self.joint_states_data
    

    def _check_rtabmap_ready(self):
        rospy.logdebug("Waiting for rtabmap pose to be READY...")
        while self.rtabmap_ready is None and not rospy.is_shutdown():
            try:
                self.tf_listener.waitForTransform(self.map_frame, self.robot_frame, rospy.Time(0), rospy.Duration(5.0))
                self.rtabmap_ready = True
                rospy.logdebug("Current rtabmap pose READY=>")

            except:
                rospy.logerr("Current rtabmap pose not ready yet, retrying for getting rtabmap pose")
        return self.rtabmap_ready
    

    def _check_publishers_connection(self):
        """
        Checks that all the publishers are working
        :return:
        """
        rate = rospy.Rate(10)  # 10hz
        while self._cmd_vel_pub.get_num_connections() == 0 and not rospy.is_shutdown():
            rospy.logdebug("No susbribers to _cmd_vel_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_cmd_vel_pub Publisher Connected")

        while self._pan_vel_pub.get_num_connections() == 0 and not rospy.is_shutdown():
            rospy.logdebug("No susbribers to _pan_vel_pub yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("_pan_vel_pub Publisher Connected")
        rospy.logdebug("All Publishers READY")


    def reset_look_at_data(self):
        self.look_at_person_data = None
        self.look_at_group_data = None
        self.look_at_position_data = None
        self.look_at_body_data = None
        self.wrong_target = False
        self.head_joint_errors = False
        self.look_at_done = False


    def reset_look_at_target(self):
        self.look_at_body_target = None
        self.look_at_group_target = None
        self.look_at_person_target = None
        self.look_at_position_target = None
        self.head_goal = False
        self.head_joint_errors = False
        self.pan_angle = 0.
        self.tilt_angle = 0.
        self.x_pan_target = 0.
        self.y_pan_target = 0.


    def reset_go_to_target(self):
        self.go_to_group_target = None
        self.go_to_body_target = None
        self.go_to_person_target = None
        self.go_to_position_target = None
        self.base_goal = False
        self.reset_controller_go_to_values()
        self.lin_vel = 0.
        self.ang_vel = 0.


    def reset_continuous_look_at_flag(self):
        self.is_continuous_look_at = False
        self.continuous_look_at_done = False


    def reset_continuous_go_to_flag(self):
        self.is_continuous_go_to = False
        self.continuous_go_to_done = False


    def reset_go_to_data(self):
        self.go_to_position_data = None
        self.go_to_body_data = None
        self.go_to_person_data = None
        self.go_to_group_data = None
        self.wrong_target = False
        self.go_to_done = False


    def reset_controller_go_to_values(self):
        with self.lock_controller:
            self.controller.controller_config.goto_target_id = -1
            self.controller.controller_config.human_target_id = -1
            self.controller.controller_config.goto_target_pose = np.zeros_like(self.controller.controller_config.goto_target_pose)
            self.controller.set_goto_target(self.state, target=-1)
            self.controller.set_mode()
            self.controller.set_weights()
            self.controller.reset_shared_values()


    def _look_at_body_callback(self, data):
        self.reset_look_at_data()
        self.look_at_body_data = data
        look_at_body_target = int(self.look_at_body_data.id[1:])
        if float(look_at_body_target) in self.state.humans_id.tolist() and look_at_body_target != -1:
            self.reset_look_at_target()
            self.reset_continuous_look_at_flag()
            self.head_goal = True
            self.look_at_body_target = look_at_body_target
            if self.look_at_body_data.continuous:
                self.is_continuous_look_at = True
        elif look_at_body_target == -1:
            # reset but not the look_at targets
            self.look_at_body_data = None
            self.look_at_done = True
            if not self.is_continuous_look_at:
                self.reset_look_at_target()
                self.reset_continuous_look_at_flag()
        else:
            self.wrong_target = True
            self.reset_look_at_target()
            self.reset_continuous_look_at_flag()


    def _look_at_person_callback(self, data):
        self.reset_look_at_data()
        self.look_at_person_data = data
        look_at_person_target = self.look_at_person_data.id
        if look_at_person_target in self.hri_data[2].keys():
            look_at_person_target = int(self.hri_data[2][look_at_person_target][1:])
            if float(look_at_person_target) in self.state.humans_id.tolist():
                self.reset_look_at_target()
                self.reset_continuous_look_at_flag()
                self.head_goal = True
                self.look_at_person_target = look_at_person_target
                if self.look_at_person_data.continuous:
                    self.is_continuous_look_at = True
            else:
                self.wrong_target = True
                self.reset_look_at_target()
                self.reset_continuous_look_at_flag()
        elif look_at_person_target == 'p-1':
            # reset but not the look_at targets
            self.look_at_person_data = None
            self.look_at_done = True
            if not self.is_continuous_look_at:
                self.reset_look_at_target()
                self.reset_continuous_look_at_flag()
        else:
            self.wrong_target = True
            self.reset_look_at_target()
            self.reset_continuous_look_at_flag()


    def _look_at_group_callback(self, data):
        self.reset_look_at_data()
        self.look_at_group_data = data
        look_at_group_target = int(self.look_at_group_data.id[3:])
        if float(look_at_group_target) in self.state.groups_id.tolist() and look_at_group_target != -1:
            self.reset_look_at_target()
            self.reset_continuous_look_at_flag()
            self.head_goal = True
            self.look_at_group_target = look_at_group_target
            if self.look_at_group_data.continuous:
                self.is_continuous_look_at = True
        elif look_at_group_target == -1:
            # reset but not the look_at targets
            self.look_at_group_data = None
            self.look_at_done = True
            if not self.is_continuous_look_at:
                self.reset_look_at_target()
                self.reset_continuous_look_at_flag()
        else:
            self.wrong_target = True
            self.reset_look_at_target()
            self.reset_continuous_look_at_flag()


    def _look_at_position_callback(self, data):
        self.reset_look_at_data()
        self.look_at_position_data = data
        look_at_position_target = np.asarray(self.look_at_position_data.look_at_position)
        if not look_at_position_target[-1]:
            self.wrong_target = True
            self.reset_look_at_target()
            self.reset_continuous_look_at_flag()
        else:
            if look_at_position_target[-1] == 1:
                self.reset_look_at_target()
                self.reset_continuous_look_at_flag()
                self.head_goal = True
                self.look_at_position_target = look_at_position_target
                if self.look_at_position_data.continuous:
                    self.is_continuous_look_at = True
            else:
                # reset but not the look_at targets
                self.look_at_position_data = None
                self.look_at_done = True
                if not self.is_continuous_look_at:
                    self.reset_look_at_target()
                    self.reset_continuous_look_at_flag()


    def _go_to_position_callback(self, data):
        self.reset_go_to_data()
        self.go_to_position_data = data
        go_to_position_target = np.asarray(self.go_to_position_data.go_to_target_pose)
        if not go_to_position_target[-1]:
            self.wrong_target = True
            self.reset_go_to_target()
            self.reset_continuous_go_to_flag()
        else:            
            if go_to_position_target[-1] == 1:
                self.reset_go_to_target()
                self.reset_continuous_go_to_flag()
                self.base_goal = True
                self.go_to_position_target = go_to_position_target
                with self.lock_controller:
                    self.controller.controller_config.goto_target_id = -1
                    self.controller.controller_config.human_target_id = -1
                    self.controller.controller_config.goto_target_pose = go_to_position_target
                    self.controller.set_goto_target(self.state, target=go_to_position_target)
                    self.controller.set_mode()
                    self.controller.set_weights()
                if self.go_to_position_data.continuous:
                    self.is_continuous_go_to = True
            else:
                # reset controller
                self.go_to_position_data = None
                self.go_to_done = True
                if not self.is_continuous_go_to:
                    self.reset_go_to_target()
                    self.reset_continuous_go_to_flag()


    def _go_to_body_callback(self, data):
        self.reset_go_to_data()
        self.go_to_body_data = data
        go_to_body_target = int(self.go_to_body_data.id[1:])
        if float(go_to_body_target) in self.state.humans_id.tolist() and go_to_body_target != -1:
            self.reset_go_to_target()
            self.reset_continuous_go_to_flag()
            self.base_goal = True
            self.go_to_body_target = go_to_body_target
            with self.lock_controller:
                self.controller.controller_config.goto_target_id = go_to_body_target
                self.controller.controller_config.human_target_id = -1
                self.controller.controller_config.goto_target_pose = np.zeros_like(self.controller.controller_config.goto_target_pose)
                self.controller.set_goto_target(self.state, target=go_to_body_target)
                self.controller.set_mode()
                self.controller.set_weights()
            if self.go_to_body_data.continuous:
                self.is_continuous_go_to = True
        elif go_to_body_target == -1:
            # reset controller
            self.go_to_body_data = None
            self.go_to_done = True
            if not self.is_continuous_go_to:
                self.reset_go_to_target()
                self.reset_continuous_go_to_flag()
        else:
            self.wrong_target = True
            self.reset_go_to_target()
            self.reset_continuous_go_to_flag()


    def _go_to_group_callback(self, data):
        self.reset_go_to_data()
        self.go_to_group_data = data
        go_to_group_target = int(self.go_to_group_data.id[3:])
        if float(go_to_group_target) in self.state.groups_id.tolist() and go_to_group_target != -1:
            self.reset_go_to_target()
            self.reset_continuous_go_to_flag()
            self.base_goal = True
            self.go_to_group_target = go_to_group_target
            with self.lock_controller:
                self.controller.controller_config.goto_target_id = go_to_group_target
                self.controller.controller_config.human_target_id = -1
                self.controller.controller_config.goto_target_pose = np.zeros_like(self.controller.controller_config.goto_target_pose)
                self.controller.set_goto_target(self.state, target=go_to_group_target, group=True)
                self.controller.set_mode()
                self.controller.set_weights()
                if self.go_to_group_data.continuous:
                    self.is_continuous_go_to = True
        elif go_to_group_target == -1:
            # reset controller
            self.go_to_group_data = None
            self.go_to_done = True
            if not self.is_continuous_go_to:
                self.reset_go_to_target()
                self.reset_continuous_go_to_flag()
        else:
            self.wrong_target = True
            self.reset_go_to_target()
            self.reset_continuous_go_to_flag()


    def _go_to_person_callback(self, data):
        self.reset_go_to_data()
        self.go_to_person_data = data
        go_to_person_target = self.go_to_person_data.id
        if go_to_person_target in self.hri_data[2].keys():
            go_to_person_target = int(self.hri_data[2][self.go_to_person_data.id][1:])        
            if float(go_to_person_target) in self.state.humans_id.tolist():
                self.reset_go_to_target()
                self.reset_continuous_go_to_flag()
                self.base_goal = True
                self.go_to_person_target = go_to_person_target
                with self.lock_controller:
                    self.controller.controller_config.goto_target_id = go_to_person_target
                    self.controller.controller_config.human_target_id = -1
                    self.controller.controller_config.goto_target_pose = np.zeros_like(self.controller.controller_config.goto_target_pose)
                    self.controller.set_goto_target(self.state, target=go_to_person_target)
                    self.controller.set_mode()
                    self.controller.set_weights()
                    if self.go_to_person_data.continuous:
                        self.is_continuous_go_to = True
            else:
                self.wrong_target = True
                self.reset_go_to_target()
                self.reset_continuous_go_to_flag()
        elif go_to_person_target == 'p-1':
            # reset controller
            self.go_to_person_data = None
            self.go_to_done = True
            if not self.is_continuous_go_to:
                self.reset_go_to_target()
                self.reset_continuous_go_to_flag()
        else:
            self.wrong_target = True
            self.reset_go_to_target()
            self.reset_continuous_go_to_flag()


    def _joint_states_callback(self, data):
        with self.lock:
            self.joint_states_data = data


    def _tracked_persons_2d_callback(self, data):
        with self.lock:
            self.tracked_persons_2d_data = data
            self.timestamp_tracking = data.header.stamp
            if self.controller.actions is not None:
                self.action_idx = int((self.timestamp_tracking - self.timestamp_tracking_latest).to_sec()/self.controller.controller_config.h)
                self.action_idx = min(self.action_idx, len(self.controller.actions) - 1)
                self.actions = self.controller.actions
                # self.publish_vel(self.controller.actions[self.action_idx])   


    def _global_map_callback(self, data):
        with self.lock:
            self.global_map_data = data
            self.timestamp_global_map_cb = self.global_map_data.header.stamp
            self.x_global_map = self.global_map_data.info.origin.position.x
            self.y_global_map = self.global_map_data.info.origin.position.y
            self.global_map_width = self.global_map_data.info.width
            self.global_map_height = self.global_map_data.info.height
            self.global_map_resolution = self.global_map_data.info.resolution
            self.global_map_size = [[self.x_global_map, self.x_global_map + self.global_map_width*self.global_map_resolution],[self.y_global_map, self.y_global_map + self.global_map_height*self.global_map_resolution]]
            self.last_shape_global_map = (self.global_map_height, self.global_map_width)
            self.global_map_static = (np.asarray(self.global_map_data.data) / 100).reshape(self.last_shape_global_map)


    def _local_map_callback(self, data):
        with self.lock:
            self.local_map_data = data
            self.x_local_map = self.local_map_data.info.origin.position.x
            self.y_local_map = self.local_map_data.info.origin.position.y
            self.local_map_width = self.local_map_data.info.width
            self.local_map_height = self.local_map_data.info.height
            self.local_map_resolution = self.local_map_data.info.resolution
            self.local_map_size = [[self.x_local_map, self.x_local_map + self.local_map_width*self.local_map_resolution],[self.y_local_map, self.y_local_map + self.local_map_height*self.local_map_resolution]]
            self.last_shape_local_map = (self.local_map_height, self.local_map_width)
            self.local_static_map = (np.asarray(self.local_map_data.data) / 100).reshape(self.last_shape_local_map)


    def get_social_cost_map(self, data):
        groups = None
        ids = np.where(self.state.humans_visible == 0)[0]
        # ids = ids[ids != self.state.humans_id.tolist().index(self.human_target_id)]
        n_human = len(ids)
        # (x, y, direction, velocity, is_sitting)
        humans = np.zeros((n_human, 5))
        for j in range(n_human):
            i = ids[j]
            global_position = self.state.humans_pose[i, :2]
            local_position = global_to_local(self.state.robot_pose,
                                                   global_position)
            local_angle = self.state.humans_pose[i, -
                                                1] - self.state.robot_pose[-1]
            humans[j, :] = * \
                local_position, local_angle, self.state.humans_velocity[i, 0], 0
        if n_human > 1:
            groups = []
            for idx, group_id in enumerate(self.state.groups_id):
                if group_id != -1:
                    center = global_to_local(self.state.robot_pose, self.state.groups_pose[idx])
                    groups.append(Group(center=center, person_ids=np.where(self.state.humans_group_id == group_id)[0]))
                else:
                    break

        f_dsz = self.ssn.calc_dsz(humans, groups=groups)

        occupancy_grid = data
        occupancy_grid.info.map_load_time = rospy.Time.now()
        occupancy_grid.data = (f_dsz * 100).flatten().astype(np.int8)
        self._local_social_cost_map_pub.publish(occupancy_grid)
        return f_dsz


    def _pub_vel_callback(self, event):
        self.hri_data = self.ros_4_hri_interface.get_data()
        if  self.head_goal or self.i == 0:
            if self.look_at_body_target or self.look_at_group_target or self.look_at_person_target or self.look_at_position_target is not None:
                self.pan_angle = constraint_angle(np.arctan2(self.y_pan_target - self.y_robot_pose, self.x_pan_target - self.x_robot_pose) - self.yaw_robot_pose) # to check
            self.publish_head_cmd(self.pan_angle, self.tilt_angle)
        if self.action_idx is not None:
            actions = self.actions[self.action_idx]
            self.lin_vel = 0.
            self.ang_vel = 0.
            if np.any(actions) or self.i == 0:
                self.publish_vel(actions)
            self.action_idx += 1
            if self.action_idx > (len(self.controller.actions) - 1):
                self.action_idx = 0
                self.actions = np.copy(self.controller.initial_action)


    def _diagnostics_callback(self, event):
        self.publish_diagnostics()


    def publish_diagnostics(self):
        diagnostic_msg = DiagnosticArray()
        diagnostic_msg.status = []
        diagnostic_msg.header.stamp = rospy.Time.now()
        status = DiagnosticStatus()
        status.name = 'Functionality: Robot Behavior: MPC Controller'
        status.values.append(KeyValue(key='Linear velocity required', value='{} m/s'.format(self.lin_vel)))
        status.values.append(KeyValue(key='Angular velocity required', value='{} rad/s'.format(self.ang_vel)))
        status.values.append(KeyValue(key='Pan angle required', value='{} rad'.format(self.pan_angle)))
        status.values.append(KeyValue(key='Tilt angle required', value='{} rad'.format(self.tilt_angle)))
        status.values.append(KeyValue(key='Top view pan target [x, y]', value='[{} {}]'.format(self.x_pan_target, self.y_pan_target)))
        status.values.append(KeyValue(key='Top view human target [x, y, theta]', value='[{} {} {}]'.format(self.x_human_features, self.y_human_features, self.th_human_features)))
        status.values.append(KeyValue(key='Top view intermediate waypoint [x, y, theta]', value='[{} {} {}]'.format(self.x_wp, self.y_wp, self.th_wp)))
        status.values.append(KeyValue(key='Top view final goal [x, y, theta]', value='[{} {} {}]'.format(self.x_final, self.y_final, self.th_final)))
        if self.controller_status_msg:
            status.values.append(KeyValue(key='Goto target id', value='{}'.format(self.controller_status_msg.goto_target_id)))
            status.values.append(KeyValue(key='Human target id', value='{}'.format(self.controller_status_msg.human_target_id)))
            status.values.append(KeyValue(key='Pan target id', value='{}'.format(self.controller_status_msg.pan_target_id)))
            status.values.append(KeyValue(key='Continuous look_at      Done', value='[{} {}]'.format(self.controller_status_msg.is_continuous_look_at, self.controller_status_msg.continuous_look_at_done)))
            status.values.append(KeyValue(key='Continuous go_to     Done', value='[{} {}]'.format(self.controller_status_msg.is_continuous_go_to, self.controller_status_msg.continuous_go_to_done)))
            status.values.append(KeyValue(key='Distance to Goto target [x (m), y (m), theta (rad), distance (m)]', value='{}'.format(self.controller_status_msg.dist_to_goto_target)))
            status.values.append(KeyValue(key='Distance to Human target [x (m), y (m), theta (rad), distance (m)]', value='{}'.format(self.controller_status_msg.dist_to_human_target)))
            status.values.append(KeyValue(key='Head joint errors [pan angle, tilt angle] in rad', value='{}'.format(self.controller_status_msg.head_joint_errors)))
        status.values.append(KeyValue(key='Number of MPC loops executed', value='{}'.format(self.i)))
        if self.hri_data:
            status.values.append(KeyValue(key='Number of tracked bodies', value='{}'.format(len(self.hri_data[0]))))
            status.values.append(KeyValue(key='Number of tracked groups', value='{}'.format(len(self.hri_data[1]))))
            status.values.append(KeyValue(key='Number of person body mapped', value='{}'.format(len(self.hri_data[2]))))
            status.values.append(KeyValue(key='Number of body person mapped', value='{}'.format(len(self.hri_data[3]))))

        # by default
        status.level = DiagnosticStatus.OK
        status.message = ''
        if self.i == 0 or self.controller_status_msg == None:
            status.level = DiagnosticStatus.WARN
            status.message = 'Initialization of the MPC Controller not finished'
        if self.hri_data:
            if len(self.hri_data[2]) != len(self.hri_data[3]):
                status.level = DiagnosticStatus.OK
                status.message = 'The number of person body mapped ({}) is different from the number of body person mapped ({})'.format(len(self.hri_data[2]), len(self.hri_data[3]))
            if not self.hri_data[0]:
                status.level = DiagnosticStatus.OK
                status.message = 'No tracked bodies as input'
            if len(self.hri_data[0]) != len(self.hri_data[2]) or len(self.hri_data[0]) != len(self.hri_data[3]):
                status.level = DiagnosticStatus.OK
                status.message += ' The number of tracked bodies ({}) is different from the number of body person mapped ({}) or from the number of person body mapped ({})'.format(len(self.hri_data[0]), len(self.hri_data[2]), len(self.hri_data[3]))
        if self.controller_status_msg:
            if self.controller_status_msg.status == ControllerStatus.IS_FAILURE:
                if self.wrong_target:
                    status.level = DiagnosticStatus.WARN
                    status.message += ' Target neither in the field of view of the robot, nor in memory'
                if self.controller.is_failure:
                    status.level += DiagnosticStatus.ERROR
                    status.message = ' MPC Controller is in a failure mode'
            if self.controller_status_msg.goto_target_id != -1 and self.lin_vel == 0. and self.ang_vel == 0.:
                status.level = DiagnosticStatus.ERROR
                status.message += ' Go_to target mode error, no velocities (lin_vel, ang_vel) computed'
            if self.controller_status_msg.pan_target_id != -1 and self.pan_angle == 0. and self.tilt_angle == 0.:
                status.level += DiagnosticStatus.ERROR
                status.message = ' Look_at target mode error, no angles (pan_angle, tilt_angle) computed'
                if self.x_pan_target == 0. and self.y_pan_target == 0.:
                    status.message += ', pan target not set because pan_target_id was not found'
                if self.x_wp == 0. and self.y_wp == 0. and self.th_wp == 0.:
                    status.message += ', shared_waypoint of the MPC controller not ready or MPC controller in a wrong mode'
                if self.x_final == 0. and self.y_final == 0. and self.th_final == 0.:
                    status.message += ', shared_target of the MPC controller not ready or can not be calculated'
            # TODO: add ERROR/WARN status for escort mode (x_human_features, y_human_features, th_human_features) 
            
        diagnostic_msg.status.append(status)
        self._diagnostics_pub.publish(diagnostic_msg)


    def shutdown(self):
        rospy.loginfo("Stopping the BehaviorGenerator")
        self.close()
        rospy.loginfo("Killing the BehaviorGenerator node")


    def close(self):
        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()
        if self._publishers:
            for publisher in self._publishers:
                if isinstance(publisher, dict):
                    for pub in publisher.values():
                        pub.unregister()
                else:
                    publisher.unregister()
        if self._timers:
            for timer in self._timers:
                timer.shutdown()
        self.ros_4_hri_interface.close()
        self.controller.close()


class RobotState:
    pass
