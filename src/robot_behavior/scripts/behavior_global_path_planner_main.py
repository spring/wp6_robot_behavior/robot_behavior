#!/usr/bin/env python3
import rospy
import pkg_resources
import yaml
from robot_behavior import GlobalPathPlannerNode


if __name__ == '__main__':
    # Init node
    rospy.init_node('GlobalPathPlannerNode', log_level=rospy.DEBUG, anonymous=True)
    controller = GlobalPathPlannerNode()
    controller.run()
    controller.shutdown()
    # rospy.spin()
