#!/usr/bin/env python3
import rospy
from robot_behavior import GoToGroupActionClient


if __name__ == '__main__':
    # Init node
    action_name = 'go_to_group_action'
    node_name = action_name + '_client'
    rospy.init_node(node_name, log_level=rospy.INFO, anonymous=True)
    client = GoToGroupActionClient(name=action_name)
    result = client.call_server()
    rospy.loginfo('Result is : {}'.format(result))
