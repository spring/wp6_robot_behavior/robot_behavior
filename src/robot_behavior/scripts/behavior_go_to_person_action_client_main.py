#!/usr/bin/env python3
import rospy
from robot_behavior import GoToPersonActionClient


if __name__ == '__main__':
    # Init node
    action_name = 'go_to_person_action'
    node_name = action_name + '_client'
    rospy.init_node(node_name, log_level=rospy.INFO, anonymous=True)
    client = GoToPersonActionClient(name=action_name)
    result = client.call_server()
    rospy.loginfo('Result is : {}'.format(result))
