#!/usr/bin/env python3
import rospy
import pkg_resources
import yaml
from robot_behavior import BehaviorGenerator


if __name__ == '__main__':
    # Init node
    rospy.init_node('BehaviorGenerator', log_level=rospy.DEBUG, anonymous=True)
    controller = BehaviorGenerator()
    controller.run()
    controller.shutdown()
    # rospy.spin()
