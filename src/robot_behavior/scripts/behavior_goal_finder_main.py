#!/usr/bin/env python3
import rospy
import pkg_resources
import yaml
from robot_behavior import GoalFinderNode


if __name__ == '__main__':
    # Init node
    rospy.init_node('GoalFinderNode', log_level=rospy.DEBUG, anonymous=True)
    controller = GoalFinderNode()
    controller.run()
    controller.shutdown()
    # rospy.spin()
