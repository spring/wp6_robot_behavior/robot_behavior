#!/usr/bin/env python3
import rospy
from robot_behavior import LookAtGroupActionServer


if __name__ == '__main__':
    # Init node
    action_name = 'look_at_group_action'
    node_name = action_name + '_server'
    rospy.init_node(node_name, log_level=rospy.INFO, anonymous=True)
    server = LookAtGroupActionServer(name=action_name)
    rospy.spin()
