#!/usr/bin/env python3
import rospy
import pkg_resources
import yaml
from robot_behavior import LocalPathPlannerMPCNode


if __name__ == '__main__':
    # Init node
    rospy.init_node('LocalPathPlannerMPCNode', log_level=rospy.DEBUG, anonymous=True)
    controller = LocalPathPlannerMPCNode()
    controller.run()
    controller.shutdown()
    # rospy.spin()
