<a name="readme-top"></a>

<!-- PROJECT SHIELDS -->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url] -->

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <!-- <a href="https://github.com/github_username/repo_name">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a> -->

<h3 align="center">Robot Behavior</h3>

<p align="center">This is the repository for Spring WP6 regarging the Robot Behavior Generator.

Main partners are : Inria Perception

This project is the [ROS](https://www.ros.org/) implementation of the Robot Behavior, i.e. the robot path planning and the head behaviors. This implementation is working in real time on the <a href="https://pal-robotics.com/robots/ari/"><strong>ARI</strong></a> robot from <a href="https://pal-robotics.com/"><strong>Pal Robotics</strong></a>.
</p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>    
    <li><a href="#usage">Usage</a>
      <ul>
        <li><a href="#ros-modules-required">ROS Modules Required</a></li>
        <li><a href="#planning-manager">Planning Manager</a></li>
        <li><a href="#look_at-actions">look_at actions</a></li>
        <li><a href="#go_to-actions">go_to actions</a></li>
        <li><a href="#local-path-planner-selection">Local Path Planner Selection</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- GETTING STARTED -->
## Getting Started

<details>
<summary>Getting Started</summary>



### Prerequisites

In order to run this ROS implementation, you can install the different dependencies manually or directly build the docker container image. On your local machine, you will need to install [ROS](http://wiki.ros.org/ROS/Installation) (if you want to install manually) or [Docker](https://docs.docker.com/engine/install/) (if you just want to build the docker container image).

First, install the [Robot_Behavior](https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior) repository and its submodules:

```sh
git clone https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior.git
git checkout master
git pull
git submodule update --init
git submodule update --recursive --remote
cd modules/human_aware_navigation_rl/
git checkout victor
git pull
cd ../Motor_Controller/
git checkout master
git pull
cd ../../src/hri_msgs
git checkout 0.8.0
git pull
cd ../occupancy_map_republisher/
git checkout main
git pull
cd ../py_spring_hri/
git checkout main
git pull
cd ../spring_msgs/
git checkout master
git pull
```

### Installation
* Manual Installation:
  ```sh
  cd modules/human_aware_navigation_rl/
  pip3 install -r requirements.txt
  pip3 install -U kaleido
  pip3 install -e .
  cd ../..
  source /opt/ros/noetic/setup.bash && catkin_make
  source devel/setup.bash
  ```
  A build and devel folder should have been created in your ROS workspace. See [ROS workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspace) tutorial for more informations.

* Docker:
  ```sh
  export DOCKER_BUILDKIT=1
  docker build -t behavior_generator --target behavior-generator .
  ```
  A docker container image, named `behavior_generator` should have been created with the tag: `latest`.

  You can run the docker container based on this images creating and running a [docker_run.sh](https://gitlab.inria.fr/spring/wp6_robot_behavior/play_gestures/-/blob/main/docker_run.sh?ref_type=heads) file like in the [Play Gestures](https://gitlab.inria.fr/spring/wp6_robot_behavior/play_gestures) repository:
  ```sh
  ./docker_run.sh
  ```
  In this bash file, you will need to change the `CONTAINER_NAME` value with the name you want for this docker container, the `DOCKER_IMAGE` value with docker image name you just put before, the `ROS_IP` value, the `ROS_MASTER_URI` value.

</details>

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- USAGE EXAMPLES -->
## Usage

<details>
<summary>Usage</summary>

The Robot Behavior project is an [actionlib](http://wiki.ros.org/actionlib) based package. You need to execute all these following steps in order to be able to run an action on the robot.

### ROS Modules Required

The Behavior Generator pipeline is at the end of the even bigger pipeline (perception pipeline and human robot interaction (HRI) person manager pipeline). Therefore, a lot of other ROS modules should be running before running our [Planning Manager](#planning-manager) which will run all the Robot Behaviors actions. The following services of the [docker-compose.yml](https://gitlab.inria.fr/spring/dockers/-/blob/devel/docker-compose.yml?ref_type=heads) file should be running:
* `hri`: you can run the `hri` pipeline by using the [registry.gitlab.inria.fr/spring/dockers/wp7_ros4hri_human_perception:latest](https://gitlab.inria.fr/spring/dockers/container_registry/1537) docker image or by manually installing [hri](https://github.com/ros4hri/hri_person_manager) repository and all its dependencies using the [README.md](https://github.com/ros4hri/hri_person_manager/blob/master/README.md)
* `tracker`: you can run the `tracker` pipeline by using the [registry.gitlab.inria.fr/spring/dockers/wp3_tracker:12.0](https://gitlab.inria.fr/spring/dockers/container_registry/1203) docker image or by manually installing [tracker](https://gitlab.inria.fr/spring/wp3_av_perception/docker-tracking/-/tree/devel?ref_type=heads) repository (devel branch ) and all its dependencies using the [README.md](https://gitlab.inria.fr/spring/wp3_av_perception/docker-tracking/-/blob/devel/README.md?ref_type=heads)
* `body_perception`: you can run the `body_perception` pipeline by using the [registry.gitlab.inria.fr/spring/dockers/body_perception:0.1.10](https://gitlab.inria.fr/spring/dockers/container_registry/1667) docker image or by manually installing [docker_body_perception](https://gitlab.inria.fr/spring/wp3_av_perception/docker_body_perception) repository and all its dependencies using the [README.md](https://gitlab.inria.fr/spring/wp3_av_perception/docker_body_perception/-/blob/main/README.md?ref_type=heads)


### Planning Manager

When all the ROS required modules are running, we will be able run all the Behavior actions on the robot by running the Planning Manager node. This node will launch all the following node:
* `behavior_go_to_position_2D_nav_goal`: this node is used to start the `look_at_position` action server when a `2D Nav Goal` is chosen in `rviz` (cf. tools)
* `behavior_social_cost_map_main`: to publish the local social cost map
* global_path_planner_action_server: global path planner action server using the [Fast Marching Method](https://pythonhosted.org/scikit-fmm/) which will get the `final_goal` tf and publish the closest `intermediate_waypoint` tf
* `goal_finder_action_server`: goal finder action server which will find the final goal in the global map and publish the `final_goal` tf
* `mpc_local_path_planner_action_server`: local path planner action server using Model Predective Control algorithm
* `occupancy_map_republisher`: this node is publishing all the dilated or not global and local maps
* `planning_manager`: Planning Manager node which control all the robot behavior actions
* `pointcloud2occmap`: this node is creating a local obstacles map using the pointcloud from the torso front camera of the robot
* `rl_local_path_planner_action_server`: local path planner action server using Deep Reinforcement Learning algorithm
* `/behavior_generator/behavior_go_to_body_action_server_main`: action server used to go to a specific body
* `/behavior_generator/behavior_go_to_group_action_server_main`: action server used to go to a specific group
* `/behavior_generator/behavior_go_to_person_action_server_main`: action server used to go to a specific person
* `/behavior_generator/behavior_go_to_position_action_server_main`: action server used to go to a specific position
* `/behavior_generator/behavior_look_at_body_action_server_main`: action server used to look at a specific body
* `/behavior_generator/behavior_look_at_group_action_server_main`: action server used to look at a specific group
* `/behavior_generator/behavior_look_at_person_action_server_main`: action server used to look at a specific person
* `/behavior_generator/behavior_look_at_position_action_server_main`: action server used to look at a specific position

To launch this node, execute the following step:
* Manual Installation:
    ```sh
    roslaunch robot_behavior main.launch
    ```
* Docker:
If you have not overwritten the entrypoint when launch the docker container as it is mentioned in the [installation](#installation) section. You don't need to do anything, it is launched at the starting. Otherwise, in the docker container you need to run the launch file as before:
    ```sh
    roslaunch robot_behavior main.launch
    ```

Now that all the ROS modules required for the navigation are running, you will be able to launch all the Robot behavior actions.

### look_at actions
Four types of `look_at` actions are possible:
* `look_at_position`: To look at a specific position

    You can launch the `look_at_position` action server in two ways:
    * Using a roslaunch:
        ```sh
        roslaunch robot_behavior behavior_look_at_position_action_client.launch look_at_position:='[x, y, global_local_flag, 1]' continuous:=False timeout:=200
        ```
        with `x` and `y`, the (x, y) position in meters in the global frame if `global_local_flag`=0 or in the robot frame if `global_local_flag`=1. The `continuous` flag is used to keep the action server alive even if the robot has already reach the goal. The `timeout` parameter will kill the action server when the timeout time will be reached.

    * Publishing directly in a topic:
        ```sh
        rostopic pub -1 /behavior_generator/look_at_position_action/goal spring_msgs/LookAtPositionActionGoal "header:
        seq: 0
        stamp:
            secs: 0
            nsecs: 0
        frame_id: ''
        goal_id:
        stamp:
            secs: 0
            nsecs: 0
        id: ''
        goal:
        look_at_position: [x, y, global_local_flag, 1]
        timeout: 200.0
        continuous: false"
        ```
        the same parameters as set above should be set

* `look_at_body`: To look at a specific body

    You can launch the `look_at_body` action server in two ways:
    * Using a roslaunch:
        ```sh
        roslaunch robot_behavior behavior_look_at_body_action_client.launch body_id:=b00001 continuous:=False timeout:=200
        ```
        with `b00001` the human body id, one of the body id you can see on `rviz` or that are published in the `/humans/bodies/tracked` topic. The `continuous` flag is used to keep the action server alive even if the robot has already reach the goal. The `timeout` parameter will kill the action server when the timeout time will be reached.

    * Publishing directly in a topic:
        ```sh
        rostopic pub -1 /behavior_generator/look_at_body_action/goal spring_msgs/LookAtEntityActionGoal "header:
        seq: 0
        stamp:
            secs: 0
            nsecs: 0
        frame_id: ''
        goal_id:
        stamp:
            secs: 0
            nsecs: 0
        id: ''
        goal:
        id: 'b00001'
        timeout: 200.0
        continuous: false"
        ```
        the same parameters as set above should be set

* `look_at_group`: To look at a specific group

    You can launch the `look_at_group` action server in two ways:
    * Using a roslaunch:
        ```sh
        roslaunch robot_behavior behavior_look_at_group_action_client.launch group_id:=grp00001 continuous:=False timeout:=200
        ```
        with `grp00001` the group id, one of the group id you can see on `rviz` or that are published in the `/humans/groups/tracked` topic. The `continuous` flag is used to keep the action server alive even if the robot has already reach the goal. The `timeout` parameter will kill the action server when the timeout time will be reached.

    * Publishing directly in a topic:
        ```sh
        rostopic pub -1 /behavior_generator/look_at_group_action/goal spring_msgs/LookAtEntityActionGoal "header:
        seq: 0
        stamp:
            secs: 0
            nsecs: 0
        frame_id: ''
        goal_id:
        stamp:
            secs: 0
            nsecs: 0
        id: ''
        goal:
        id: 'grp00001'
        timeout: 200.0
        continuous: false"
        ```
        the same parameters as set above should be set
    
* `look_at_person`: To look at a specific person

    You can launch the `look_at_person` action server in two ways:
    * Using a roslaunch:
        ```sh
        roslaunch robot_behavior behavior_look_at_person_action_client.launch person_id:=anonymous_person_gcfbh continuous:=False timeout:=200
        ```
        with `anonymous_person_gcfbh` the person id, one of the person id you can see on `rviz` or that are published in the `/humans/persons/tracked` topic. The `continuous` flag is used to keep the action server alive even if the robot has already reach the goal. The `timeout` parameter will kill the action server when the timeout time will be reached.

    * Publishing directly in a topic:
        ```sh
        rostopic pub -1 /behavior_generator/look_at_person_action/goal spring_msgs/LookAtEntityActionGoal "header:
        seq: 0
        stamp:
            secs: 0
            nsecs: 0
        frame_id: ''
        goal_id:
        stamp:
            secs: 0
            nsecs: 0
        id: ''
        goal:
        id: 'anonymous_person_gcfbh'
        timeout: 200.0
        continuous: false"
        ```
        the same parameters as set above should be set

### go_to actions
Four types of `go_to` actions are possible:
* `go_to_position`: To go to a specific position

    You can launch the `go_to_position` action server in two ways:
    * Using a roslaunch:
        ```sh
        roslaunch robot_behavior behavior_go_to_position_action_client.launch go_to_target_pose:='[x, y, , ang, global_local_flag, 1]' continuous:=False timeout:=200
        ```
        with `x`, `y` and `ang`, the (x, y, ang ) pose in the global frame if `global_local_flag`=0 or in the robot frame if `global_local_flag`=1. The `continuous` flag is used to keep the action server alive even if the robot has already reach the goal. The `timeout` parameter will kill the action server when the timeout time will be reached.

    * Publishing directly in a topic:
        ```sh
        rostopic pub -1 /behavior_generator/go_to_position_action/goal spring_msgs/GoToPositionActionGoal "header:
        seq: 0
        stamp:
            secs: 0
            nsecs: 0
        frame_id: ''
        goal_id:
        stamp:
            secs: 0
            nsecs: 0
        id: ''
        goal:
        go_to_position: [x, y, ang, global_local_flag, 1]
        timeout: 200.0
        continuous: false"
        ```
        the same parameters as set above should be set

* `go_to_body`: To go to a specific body

    You can launch the `go_to_body` action server in two ways:
    * Using a roslaunch:
        ```sh
        roslaunch robot_behavior behavior_go_to_body_action_client.launch body_id:=b00001 continuous:=False timeout:=200
        ```
        with `b00001` the human body id, one of the body id you can see on `rviz` or that are published in the `/humans/bodies/tracked` topic. The `continuous` flag is used to keep the action server alive even if the robot has already reach the goal. The `timeout` parameter will kill the action server when the timeout time will be reached.

    * Publishing directly in a topic:
        ```sh
        rostopic pub -1 /behavior_generator/go_to_body_action/goal spring_msgs/GoToEntityActionGoal "header:
        seq: 0
        stamp:
            secs: 0
            nsecs: 0
        frame_id: ''
        goal_id:
        stamp:
            secs: 0
            nsecs: 0
        id: ''
        goal:
        id: 'b00001'
        timeout: 200.0
        continuous: false"
        ```
        the same parameters as set above should be set

* `go_to_group`: To go to a specific group

    You can launch the `go_to_group` action server in two ways:
    * Using a roslaunch:
        ```sh
        roslaunch robot_behavior behavior_go_to_group_action_client.launch group_id:=grp00001 continuous:=False timeout:=200
        ```
        with `grp00001` the group id, one of the group id you can see on `rviz` or that are published in the `/humans/groups/tracked` topic. The `continuous` flag is used to keep the action server alive even if the robot has already reach the goal. The `timeout` parameter will kill the action server when the timeout time will be reached.

    * Publishing directly in a topic:
        ```sh
        rostopic pub -1 /behavior_generator/go_to_group_action/goal spring_msgs/GoTotEntityActionGoal "header:
        seq: 0
        stamp:
            secs: 0
            nsecs: 0
        frame_id: ''
        goal_id:
        stamp:
            secs: 0
            nsecs: 0
        id: ''
        goal:
        id: 'grp00001'
        timeout: 200.0
        continuous: false"
        ```
        the same parameters as set above should be set
    
* `go_to_person`: To go to a specific person

    You can launch the `go_to_person` action server in two ways:
    * Using a roslaunch:
        ```sh
        roslaunch robot_behavior behavior_go_to_person_action_client.launch person_id:=anonymous_person_gcfbh continuous:=False timeout:=200
        ```
        with `anonymous_person_gcfbh` the person id, one of the person id you can see on `rviz` or that are published in the `/humans/persons/tracked` topic. The `continuous` flag is used to keep the action server alive even if the robot has already reach the goal. The `timeout` parameter will kill the action server when the timeout time will be reached.

    * Publishing directly in a topic:
        ```sh
        rostopic pub -1 /behavior_generator/go_to_person_action/goal spring_msgs/GoToEntityActionGoal "header:
        seq: 0
        stamp:
            secs: 0
            nsecs: 0
        frame_id: ''
        goal_id:
        stamp:
            secs: 0
            nsecs: 0
        id: ''
        goal:
        id: 'anonymous_person_gcfbh'
        timeout: 200.0
        continuous: false"
        ```
        the same parameters as set above should be set

__note__: One `look_at` action and one `go_to` action can be run at the time. If a `go_to` actions is launched while one is running, the running action will be canceled and the new one will be run.

### Local Path Planner Selection
In order to select the  Deep Reinforcement Learning (DRL) local path planning algorithm or he Model Predictive Control (MPC) local path planning algorithm, you can set a parameter using [rosparam](http://wiki.ros.org/rosparam):

```sh
rosparam set /planning_manager/local_path_planner_name social_mpc
```
use `social_mpc` you want to choose the MPC algorithm or `social_rl` you want to choose the DRl algorithm. By default, the DRL is used.

In order to check the local path planner algorithm, type:
```sh
rosparam get /planning_manager/local_path_planner_name
```
the output should be `social_mpc` or `social_rl`.

Below, we can see two resulting examples of the Behavior Generator running in real time in ARI robot through ROS:

<div align="center">
Robot Social Navigation using the Model Predictive Control (MPC) path planning algorithm

![](documentation/kdenlive_vela_rviz_and_scene.mp4)

![](documentation/kdenlive_vela_rviz_only.mp4)

Robot Social Navigation using the Deep Reinforcement Learning (DRL) path planning algorithm

</div>

</details>

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- ROADMAP -->
## Roadmap

- [ ] Feature 1
- [ ] Feature 2
- [ ] Feature 3
    - [ ] Nested Feature

See the [open issues](https://gitlab.inria.fr/robotlearn/gestures_generation/-/issues) for a full list of proposed features (and known issues).

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- CONTACT -->
## Contact

[Alex Auternaud](https://www.linkedin.com/public-profile/settings?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_self_edit_contact-info%3BiBJRsSXiRBaX2%2B4fc1LSWA%3D%3D) - alex.auternaud@inria.fr - alex.auternaud07@gmail.com

Project Link: [https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior](https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior)

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* []()
* []()
* []()

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/github_username/repo_name.svg?style=for-the-badge
[contributors-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/graphs/master?ref_type=heads
[forks-shield]: https://img.shields.io/github/forks/github_username/repo_name.svg?style=for-the-badge
[forks-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/forks
[stars-shield]: https://img.shields.io/github/stars/github_username/repo_name.svg?style=for-the-badge
[stars-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/starrers
[issues-shield]: https://img.shields.io/github/issues/github_username/repo_name.svg?style=for-the-badge
[issues-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/issues
[license-shield]: https://img.shields.io/github/license/github_username/repo_name.svg?style=for-the-badge
[license-url]: https://github.com/github_username/repo_name/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username