FROM nvidia/cuda:11.7.1-cudnn8-devel-ubuntu20.04 as base-ros-gpu
SHELL ["/bin/bash", "-c"]
WORKDIR /
RUN echo "Setting up timezone..." && \
    echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime

# Avoid update of CUDA package
RUN rm /etc/apt/sources.list.d/cuda.list
# RUN rm /etc/apt/sources.list.d/nvidia-ml.list

RUN echo "Installing Python and Pytorch..." && \
    apt-get update && \
    apt-get install -q -y --no-install-recommends \
        python3-dev \
        python3-pip \
        tzdata \
        wget vim tmux unzip && \
    pip3 install torch torchvision torchaudio && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /workspace

RUN echo "Installing ROS Noetic..." && \
    apt-get update && \
    apt-get install -q -y --no-install-recommends \
        curl && \
    echo "deb http://packages.ros.org/ros/ubuntu focal main" > \
        /etc/apt/sources.list.d/ros-latest.list && \
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | \
        apt-key add - && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        ros-noetic-desktop && \
    rm -rf /var/lib/apt/lists/* && \
    echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc

FROM base-ros-gpu as ros-common
RUN apt-get update && apt-get install -y python3-pip \
                                         libeigen3-dev \
                                         python3-rospy \
                                         ros-noetic-tf \
                                         ros-noetic-cv-bridge \
			                             ros-noetic-perception \
                                         ros-noetic-image-transport-plugins \
					                     ros-noetic-audio-common-msgs


FROM ros-common as ros-socialmpc
RUN mkdir -p /home/ros/robot_behavior_ws
# MPC Navigation : Alex
ADD modules/Motor_Controller /home/ros/robot_behavior_ws/modules/Motor_Controller
WORKDIR /home/ros/robot_behavior_ws/modules/Motor_Controller
RUN pip3 install pyparsing==2.4.7
RUN pip3 install -r requirements.txt
RUN pip3 install -e .
RUN pip install jaxlib==0.1.73+cuda11.cudnn805 -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html
# DRL Navigation : Victor Sanchez
ADD modules/human_aware_navigation_rl/ /home/ros/robot_behavior_ws/modules/human_aware_navigation_rl/
WORKDIR /home/ros/robot_behavior_ws/modules/human_aware_navigation_rl
RUN pip3 install -r requirements.txt
RUN pip3 install -U kaleido
RUN pip3 install -e .

FROM ros-socialmpc as behavior-generator
RUN groupadd -r ros && useradd -r -g ros ros
RUN chown -R ros:ros /home/ros
USER ros
ADD --chown=ros:ros src/robot_behavior /home/ros/robot_behavior_ws/src/robot_behavior
WORKDIR /home/ros/robot_behavior_ws/
ADD src/spring_msgs src/spring_msgs
ADD src/hri_msgs src/hri_msgs
ADD src/py_spring_hri src/py_spring_hri
ADD src/occupancy_map_republisher src/occupancy_map_republisher
RUN /bin/bash -c "source /opt/ros/noetic/setup.bash && catkin_make"

COPY ./scripts/entrypoint.sh /
RUN /bin/bash -c "echo 'source /opt/ros/noetic/setup.bash' >> ~/.bashrc"
RUN /bin/bash -c "echo 'source /home/ros/robot_behavior_ws/devel/setup.bash' >> ~/.bashrc"
ENTRYPOINT ["/entrypoint.sh", "roslaunch", "robot_behavior", "main.launch"]
CMD [""]
